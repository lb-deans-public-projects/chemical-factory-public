package table_models;

import entity.EmployeeSalary;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author rajaa
 */
public class EmployeesAdvanceTableModel extends AbstractTableModel{

    Vector<EmployeeSalary> vectorEmployeesAdvance;

    public EmployeesAdvanceTableModel(Vector<EmployeeSalary> vectorEmployeesAdvance) {
        this.vectorEmployeesAdvance = vectorEmployeesAdvance;
    }
    
    @Override
    public int getRowCount() {
      return vectorEmployeesAdvance.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }
    
    @Override
    public String getColumnName(int columnIndex) {
        String columnName = "";
        switch(columnIndex) {
            case 0:
                columnName = "ID"; break;
            case 1:
                columnName = "Employee Name"; break;
            case 2:
                columnName = "CNIC/Passport"; break;
            case 3:
                columnName = "Date of Advance"; break;
            case 4:
                columnName = "Advance Amount"; break;
        }
        
        return columnName;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object object = "";
        switch(columnIndex) {
            case 0:
                object = vectorEmployeesAdvance.get(rowIndex).getEmployee().getEmployeeId();
                break;
            case 1:
                object = vectorEmployeesAdvance.get(rowIndex).getEmployee().getEmployeeName();
                break;
            case 2:
                object = vectorEmployeesAdvance.get(rowIndex).getEmployee().getCnicPassport();
                break;
            case 3:
                object = vectorEmployeesAdvance.get(rowIndex).getSelectDate();
                break;
            case 4:
                object = vectorEmployeesAdvance.get(rowIndex).getEmployeeAdvanceAmount();
                break;
        }
        return object;
    }

    
    public Vector<EmployeeSalary> getVectorEmployeeAdvance() {
        return vectorEmployeesAdvance;
    }
    
}
