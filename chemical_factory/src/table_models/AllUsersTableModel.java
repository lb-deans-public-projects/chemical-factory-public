/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table_models;

import entity.User;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Irfan Ullah
 */
public class AllUsersTableModel extends AbstractTableModel {
    Vector<User> vectorUsers;

    public AllUsersTableModel(Vector<User> vectorUser) {
        this.vectorUsers = vectorUser;
    }

    @Override
    public int getRowCount() {
       return vectorUsers.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public String getColumnName(int columnIndex){
       String columnName = "";
       switch(columnIndex){
           case 0:
               columnName = "S.NO";
               break;
           case 1:
               columnName = "Display Name";
               break;
           case 2:
               columnName = "User Name";
               break;
               
       }
       return columnName;
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object object = "";
        
        switch(columnIndex){
            case 0:
//                object = vectorUsers.get(rowIndex).getUserId();
                object = vectorUsers.get(rowIndex).getSerialNumber();
                break;
            case 1:
                object = vectorUsers.get(rowIndex).getDisplayName();
                break;
            case 2:
                object = vectorUsers.get(rowIndex).getUserName();
        }
        return object;
        
    }
    public Vector<User> getVectorUser(){
        return this.vectorUsers;
    }
    
    
}
