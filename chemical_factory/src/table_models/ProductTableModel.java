/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table_models;

import entity.Product;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;

public class ProductTableModel extends AbstractTableModel {

    private Vector<Product> vectorProduct;

    public ProductTableModel(Vector<Product> vectorProduct) {
        this.vectorProduct = vectorProduct;
    }
    
    @Override
    public int getRowCount() {
        return vectorProduct.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    public String getColumnName(int columnIndex) {
        String columnName = "";
        switch (columnIndex) {
            case 0:
                columnName = "S.NO";
                break;

            case 1:
                columnName = "Name";
                break;

        }
        return columnName;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object object = "";
        
        switch (columnIndex) {
            case 0:
//                object = vectorProduct.get(rowIndex).getProductId();
                object = vectorProduct.get(rowIndex).getSerialNumber();
                break;
            case 1:
                object = vectorProduct.get(rowIndex).getProductName();
                break;
            
        }
        return object;
    }

    public Vector<Product> getVectorProduct() {
        return vectorProduct;
    }

    public void setVectorProduct(Vector<Product> vectorProduct) {
        this.vectorProduct = vectorProduct;
    }

    
}
