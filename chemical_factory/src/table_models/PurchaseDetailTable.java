package table_models;

import entity.Item;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;

public class PurchaseDetailTable extends AbstractTableModel{

    private Vector<Item> vectorPurchaseDetail;

    public PurchaseDetailTable(Vector<Item> vectorPurchaseDetail) {
        this.vectorPurchaseDetail = vectorPurchaseDetail;
    }
    
    @Override
    public int getRowCount() {
        return vectorPurchaseDetail.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    public String getColumnName(int columnIndex) {
        String columnName = "";
        switch (columnIndex) {
            case 0:
                columnName = "S.NO";
                break;

            case 1:
                columnName = "Item Name";
                break;
            case 2:
                columnName = "Unit Price";
                break;
            case 3:
                columnName = "Quantity";
                break;
            case 4:
                columnName = "Total";
                break;

        }
        return columnName;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object object = "";
        
        switch (columnIndex) {
            case 0:
//                object = vectorProductDetail.get(rowIndex).getProductId();
                object = vectorPurchaseDetail.get(rowIndex).getSerialNumber();
                break;
            case 1:
                object = vectorPurchaseDetail.get(rowIndex).getItemName();
                break;
            case 2:
                object = vectorPurchaseDetail.get(rowIndex).getPricePerKg();
                break;
            case 3:
                object = vectorPurchaseDetail.get(rowIndex).getQuantity();
                break;
            case 4:
                object = vectorPurchaseDetail.get(rowIndex).getTotalAmount();
                break;
            
        }
        return object;
    }

    public Vector<Item> getVectorPurchaseDetail() {
        return vectorPurchaseDetail;
    }

    public void setVectorPurchaseDetail(Vector<Item> vectorPurchaseDetail) {
        this.vectorPurchaseDetail = vectorPurchaseDetail;
    }

}
