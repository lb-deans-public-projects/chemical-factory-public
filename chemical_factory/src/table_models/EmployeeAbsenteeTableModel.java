/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table_models;

import entity.EmployeeSalary;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;
import jdk.nashorn.internal.parser.TokenType;

/**
 *
 * @author Irfan Ullah
 */
public class EmployeeAbsenteeTableModel extends AbstractTableModel { 
    
    Vector<EmployeeSalary> vectorEmployeeAbsentee;

    public EmployeeAbsenteeTableModel(Vector<EmployeeSalary> vectorEmployeeAbsentee) {
        this.vectorEmployeeAbsentee = vectorEmployeeAbsentee;
    }
    

    @Override
    public int getRowCount() {
        return vectorEmployeeAbsentee.size();
    }

    @Override
    public int getColumnCount() { 
        return 5;
    }
    
    public String getColumnName(int columnIndex) {
        String columnName = "";
        switch (columnIndex) {
            case 0:
                columnName = "ID"; break;
            case 1:
                columnName = "Employee Name"; break;
            case 2:
                columnName = "CNIC/Passport"; break;
            case 3:
                columnName = "Date Of Absentee"; break;
            case 4:
                columnName = "Absentees"; break;
    }
        return columnName;
    }
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object object = "";
        switch(columnIndex) {
            case 0:
                object = vectorEmployeeAbsentee.get(rowIndex).getEmployee().getEmployeeId();
                break;
            case 1:
                object = vectorEmployeeAbsentee.get(rowIndex).getEmployee().getEmployeeName();
                break;
            case 2:
                object = vectorEmployeeAbsentee.get(rowIndex).getEmployee().getCnicPassport();
                break;
            case 3:
                object = vectorEmployeeAbsentee.get(rowIndex).getSelectDate();
                break;
            case 4:
                object = vectorEmployeeAbsentee.get(rowIndex).getNumberOfDay();
                break;
        }
        return object;
    }
    
}
