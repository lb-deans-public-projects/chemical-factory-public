/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table_models;

import entity.Product;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;


public class DailySalesTableModel extends AbstractTableModel {
    private Vector<Product> vectorProductSales;

    public DailySalesTableModel(Vector<Product> vectorProductSales) {
        this.vectorProductSales = vectorProductSales;
    }
    
    @Override
    public int getRowCount() {
        return vectorProductSales.size();
    }

    @Override
    public int getColumnCount() {
        return 7;
    }

    public String getColumnName(int columnIndex) {
        String columnName = "";
        switch (columnIndex) {
            case 0:
                columnName = "S.No";
                break;

            case 1:
                columnName = "Product Name";
                break;

            case 2:
                columnName = "Size";
                break;
            case 3:
                columnName = "Color";
                break;
            case 4:
                columnName = "Unit Price";
                break;
            case 5:
                columnName = "Quantity";
                break;
            case 6:
                columnName = "Total";
                break;
        }
        return columnName;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object object = null;
        
        switch (columnIndex) {
            case 0:
                object = (rowIndex + 1);
                break;
            case 1:
                object = vectorProductSales.get(rowIndex).getProductName();
                break;
            case 2:
                object = vectorProductSales.get(rowIndex).getSize();
                break;
            case 3:
                object = vectorProductSales.get(rowIndex).getColor();
                break;
            case 4:
                object = vectorProductSales.get(rowIndex).getPricePerKg();
                break;
            case 5:
                object = vectorProductSales.get(rowIndex).getQuantity();
                break;
            case 6:
                object = vectorProductSales.get(rowIndex).getTotalAmount();
        }
        return object;
    }

    public Vector<Product> getVectorProductSales() {
        return vectorProductSales;
    }

    public void setVectorProductSales(Vector<Product> vectorProductSales) {
        this.vectorProductSales = vectorProductSales;
    }

    
}
