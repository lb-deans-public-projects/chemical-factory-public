package table_models;

import entity.EmployeeSalary;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author rajaa
 */
public class EmployeeOvertimeTableModel extends AbstractTableModel {
    
    Vector<EmployeeSalary> vectorEmployeeOvertime;

    public EmployeeOvertimeTableModel(Vector<EmployeeSalary> vectorEmployeeOvertime) {
        this.vectorEmployeeOvertime = vectorEmployeeOvertime;
    }
    
    @Override
    public int getRowCount() {
      return vectorEmployeeOvertime.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }
    
    @Override
    public String getColumnName(int columnIndex) {
        String columnName = "";
        switch(columnIndex) {
            case 0:
                columnName = "ID"; break;
            case 1:
                columnName = "Employee Name"; break;
            case 2:
                columnName = "CNIC/Passport"; break;
            case 3:
                columnName = "Overtime Date"; break;
            case 4:
                columnName = "Overtime Hours"; break;
        }
        
        return columnName;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object object = "";
        switch(columnIndex) {
            case 0:
                object = vectorEmployeeOvertime.get(rowIndex).getEmployee().getEmployeeId();
                break;
            case 1:
                object = vectorEmployeeOvertime.get(rowIndex).getEmployee().getEmployeeName();
                break;
            case 2:
                object = vectorEmployeeOvertime.get(rowIndex).getEmployee().getCnicPassport();
                break;
            case 3:
                object = vectorEmployeeOvertime.get(rowIndex).getSelectDate();
                break;
            case 4:
                object = vectorEmployeeOvertime.get(rowIndex).getNumberOfHours();
                break;
        }
        return object;
    }

    
    public Vector<EmployeeSalary> getVectorEmployeeOvertime() {
        return vectorEmployeeOvertime;
    }
}
