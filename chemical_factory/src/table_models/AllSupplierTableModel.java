/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table_models;
import entity.Supplier;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Haroon Afridi
 */
public class AllSupplierTableModel  extends AbstractTableModel{

      private Vector<Supplier> vectorSuppliers;

    public AllSupplierTableModel(Vector<Supplier> vectorSupplier) {
        this.vectorSuppliers = vectorSupplier;
    }
    
    

    public int getRowCount() {
        return vectorSuppliers.size();
    }

    public int getColumnCount() {
        return 5;
    }

    public String getColumnName(int columnIndex) {
        String columnName = "";

        switch (columnIndex) {
            case 0:
                columnName = "ID";
                break;

            case 1:
                columnName = "Name";
                break;
            case 2:
                columnName = "Address";
                break;

            case 3:
                columnName = "Phone Number";
                break;
            case 4:
                columnName = "CNIC";
                break;

        }

        return columnName;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        Object object = null;

        switch (columnIndex) {
            case 0:
                object = vectorSuppliers.get(rowIndex).getSupplierId();
                break;
            case 1:
                object = vectorSuppliers.get(rowIndex).getSupplierName();
                break;
            case 2:
                object = vectorSuppliers.get(rowIndex).getAddress();
                break;

            case 3:
                object = vectorSuppliers.get(rowIndex).getPhoneNumber();
                break;
            case 4:
                object = vectorSuppliers.get(rowIndex).getCnicPassport();
                break;
        }
        return object;
    }

    public void setVectorSupplier(Vector<Supplier> vectorSuppliers) {
        this.vectorSuppliers = vectorSuppliers;
    }
    
    public Vector<Supplier> getVectorSupplier() {
        return vectorSuppliers;
    }

    
}
