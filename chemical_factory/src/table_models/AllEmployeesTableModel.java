package table_models;

import entity.Employee;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;


public class AllEmployeesTableModel extends AbstractTableModel {
    private Vector<Employee> vectorEmployees;

    public AllEmployeesTableModel(Vector<Employee> vectorEmployees) {
        this.vectorEmployees = vectorEmployees;
        
        
    }
    
    
    @Override
    public int getRowCount() {
        return vectorEmployees.size();
    }

    @Override
    public int getColumnCount() {
        return 8;
    }

    
    @Override
    public String getColumnName(int columnIndex) {
        String columnName = "";
        
        switch(columnIndex) {
            case 0:
                columnName = "ID";
                break;
                
            case 1:
                columnName = "Name";
                break;
                
            case 2:
                columnName = "Address";
                break;
                
            case 3:
                columnName = "DOB";
                break;
                
            case 4:
                columnName = "Phone Number";
                break;
                
            case 5:
                columnName = "CINC/Passport";
                break;
                
            case 6:
                columnName = "Employee Number";
                break;
            case 7:
                columnName = "Basic pay";
                break;
                
        }
        
        return columnName;
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object object = null;
        
        switch(columnIndex) {
            case 0:
                object = vectorEmployees.get(rowIndex).getEmployeeId();
                break;
                
            case 1:
                object = vectorEmployees.get(rowIndex).getEmployeeName();
                break;
                
            case 2:
                object = vectorEmployees.get(rowIndex).getAddress();
                break;
                
            case 3:
                object = vectorEmployees.get(rowIndex).getDateOfBirth();
                break;
                
            case 4:
                object = vectorEmployees.get(rowIndex).getPhoneNumber();
                break;
                
            case 5:
                object = vectorEmployees.get(rowIndex).getCnicPassport();
                break;
                
            case 6:
                object = vectorEmployees.get(rowIndex).getEmployeeNumber();
                break;                
                
            case 7:
                object = vectorEmployees.get(rowIndex).getBasicPay();
                break;                
                
        }
        
        return object;
    }

    public Vector<Employee> getVectorEmployees() {
        return vectorEmployees;
    }

    public void setVectorEmployees(Vector<Employee> vectorEmployees) {
        this.vectorEmployees = vectorEmployees;
    }
    
    
}
