package table_models;

import entity.Product;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;


public class ProductsProductionDetailTable extends AbstractTableModel {

     private Vector<Product> vectorProductCostDetail;

    public ProductsProductionDetailTable(Vector<Product> vectorProductCostDetail) {
        this.vectorProductCostDetail = vectorProductCostDetail;
    }
    
    @Override
    public int getRowCount() {
        return vectorProductCostDetail.size();
    }

    @Override
    public int getColumnCount() {
        return 8;
    }

    public String getColumnName(int columnIndex) {
        String columnName = "";
        switch (columnIndex) {
            case 0:
                columnName = "S.NO";
                break;

            case 1:
                columnName = "Product Name";
                break;
            case 2:
                columnName = "Color";
                break;
            case 3:
                columnName = "Size";
                break;
            case 4:
                columnName = "Insertion Date";
                break;
            case 5:
                columnName = "Total Expense";
                break;
            case 6:
                columnName = "Quantity Produced";
                break;
            case 7:
                columnName = "Price Per(KG)";
                break;

        }
        return columnName;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object object = "";
        
        switch (columnIndex) {
            case 0:
//                object = vectorProduct.get(rowIndex).getProductId();
                object = vectorProductCostDetail.get(rowIndex).getSerialNumber();
                break;
            case 1:
                object = vectorProductCostDetail.get(rowIndex).getProductName();
                break;
            case 2:
                object = vectorProductCostDetail.get(rowIndex).getColor();
                break;
            case 3:
                object = vectorProductCostDetail.get(rowIndex).getSize();
                break;
            case 4:
                object = vectorProductCostDetail.get(rowIndex).getData();
                break;
            case 5:
                object = vectorProductCostDetail.get(rowIndex).getTotalExpense();
                break;
            case 6:
                object = vectorProductCostDetail.get(rowIndex).getQuantity();
                break;
            case 7:
                object = vectorProductCostDetail.get(rowIndex).getPricePerKg();
                break;
            
        }
        return object;
    }

    public Vector<Product> getVectorProductCostDetail() {
        return vectorProductCostDetail;
    }

    public void setVectorProductCostDetail(Vector<Product> vectorProductCostDetail) {
        this.vectorProductCostDetail = vectorProductCostDetail;
    }

}
