/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table_models;

import entity.Customer;
import entity.CustomerPayment;
import entity.CustomerPaymentHistory;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;

public class CustomerPaymentHistoryTableModel extends AbstractTableModel {

    private Vector<CustomerPayment> vectorCustomerPaymentHistory;

    public CustomerPaymentHistoryTableModel(Vector<CustomerPayment> vectorCustomerPaymentHistory) {
        this.vectorCustomerPaymentHistory = vectorCustomerPaymentHistory;
    }

    @Override
    public int getRowCount() {
        return vectorCustomerPaymentHistory.size();
    }

    @Override
    public int getColumnCount() {
        return 7;
    }

    @Override
    public String getColumnName(int columnIndex) {
        String columnName = "";
        switch (columnIndex) {
            case 0:
                columnName = "S.No";
                break;

            case 1:
                columnName = "Date";
                break;
            case 2:
                columnName = "Paid Amount";
                break;

            case 3:
                columnName = "Payment Method";
                break;

            case 4:
                columnName = "Cheque#";
                break;

            case 5:
                columnName = "Bank Name";
                break;
            case 6:
                columnName = "Branch Name";
                break;

        }
        return columnName;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object object = null;

        switch (columnIndex) {
            case 0:
                object = (rowIndex + 1);
                break;

            case 1:
                object = vectorCustomerPaymentHistory.get(rowIndex).getDate();
                break;

            case 2:
                object = vectorCustomerPaymentHistory.get(rowIndex).getCustomerAmountPay();
                break;

            case 3:
                object = vectorCustomerPaymentHistory.get(rowIndex).getPaymentMethod().getType();
                break;
            case 4:
                object = vectorCustomerPaymentHistory.get(rowIndex).getPaymentMethod().getChequeNumber() + "";
                if (object.equals("null")) {
                    object = "";
                }
                break;
            case 5:
                object = vectorCustomerPaymentHistory.get(rowIndex).getPaymentMethod().getBankName() + "";
                if (object.equals("null")) {
                    object = "";
                }
                break;
            case 6:
                object = vectorCustomerPaymentHistory.get(rowIndex).getPaymentMethod().getBranchName() + "";
                if (object.equals("null")) {
                    object = "";
                }
                break;

        }
        return object;
    }

    public Vector<CustomerPayment> getVectorCustomerPaymentHistory() {
        return vectorCustomerPaymentHistory;
    }

    public void setVectorCustomerPaymentHistory(Vector<CustomerPaymentHistory> vectorCustomerPaymentHistorys) {
        this.vectorCustomerPaymentHistory = vectorCustomerPaymentHistory;
    }

}
