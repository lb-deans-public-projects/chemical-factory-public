/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table_models;

import entity.Customer;
import entity.Supplier;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Net Valley
 */
public class AllCustomerTableModel extends AbstractTableModel {

    private Vector<Customer> vectorCustomer;

    public AllCustomerTableModel(Vector<Customer> vectorCustomer) {
        this.vectorCustomer = vectorCustomer;
    }

    public int getRowCount() {
        return vectorCustomer.size();
    }

    public int getColumnCount() {
        return 5;
    }

    public String getColumnName(int columnIndex) {
        String columnName = "";

        switch (columnIndex) {
            case 0:
                columnName = "ID";
                break;

            case 1:
                columnName = "Name";
                break;
            case 2:
                columnName = "CNIC";
                break;

            case 3:
                columnName = "Address";
                break;
            case 4:
                columnName = "Phone Number";
                break;

        }

        return columnName;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        Object object = null;

        switch (columnIndex) {
            case 0:
                object = vectorCustomer.get(rowIndex).getcustomerId();
                break;
            case 1:
                object = vectorCustomer.get(rowIndex).getName();
                break;
            case 2:
                object = vectorCustomer.get(rowIndex).getCnic();
                break;

            case 3:
                object = vectorCustomer.get(rowIndex).getAddress();
                break;
            case 4:
                object = vectorCustomer.get(rowIndex).getPhoneNumber();
                break;
        }
        return object;
    }

    public void setVectorCustomer(Vector<Customer> vectorCustomer) {
        this.vectorCustomer = vectorCustomer;
    }

    public Vector<Customer> getVectorCustomer() {
        return vectorCustomer;
    }

}
