package table_models;

import entity.Item;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Irfan Ullah
 */
public class ItemTableModel extends AbstractTableModel{
    
    private Vector<Item> vectorItem;

    public ItemTableModel(Vector<Item> vectorItem) {
        this.vectorItem = vectorItem;
    }
    
    @Override
    public int getRowCount() {
        return vectorItem.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    public String getColumnName(int columnIndex) {
        String columnName = "";
        switch (columnIndex) {
            case 0:
                columnName = "S.NO";
                break;

            case 1:
                columnName = "Supplier Name";
                break;
            case 2:
                columnName = "Item Name";
                break;

        }
        return columnName;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object object = "";
        
        switch (columnIndex) {
            case 0:
//                object = vectorProduct.get(rowIndex).getProductId();
                object = vectorItem.get(rowIndex).getSerialNumber();
                break;
            case 1:
                object = vectorItem.get(rowIndex).getSupplier().getSupplierName();
                break;
                 case 2:
                object = vectorItem.get(rowIndex).getItemName();
                break;
            
        }
        return object;
    }

    public Vector<Item> getVectorItem() {
        return vectorItem;
    }

    public void setVectorItem(Vector<Item> vectorItem) {
        this.vectorItem = vectorItem;
    }
}
