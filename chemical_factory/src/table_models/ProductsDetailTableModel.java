package table_models;

import entity.Product;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author rajaa
 */
public class ProductsDetailTableModel extends AbstractTableModel {
    
    private Vector<Product> vectorProductDetail;

    public ProductsDetailTableModel(Vector<Product> vectorProductDetail) {
        this.vectorProductDetail = vectorProductDetail;
    }
    
    @Override
    public int getRowCount() {
        return vectorProductDetail.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    public String getColumnName(int columnIndex) {
        String columnName = "";
        switch (columnIndex) {
            case 0:
                columnName = "S.NO";
                break;

            case 1:
                columnName = "Product Name";
                break;
            case 2:
                columnName = "Size";
                break;
            case 3:
                columnName = "Color";
                break;
            case 4:
                columnName = "Price Per Unit";
                break;

        }
        return columnName;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object object = "";
        
        switch (columnIndex) {
            case 0:
//                object = vectorProductDetail.get(rowIndex).getProductId();
                object = vectorProductDetail.get(rowIndex).getSerialNumber();
                break;
            case 1:
                object = vectorProductDetail.get(rowIndex).getProductName();
                break;
            case 2:
                object = vectorProductDetail.get(rowIndex).getSize();
                break;
            case 3:
                object = vectorProductDetail.get(rowIndex).getColor();
                break;
            case 4:
                object = vectorProductDetail.get(rowIndex).getPricePerKg();
                break;
            
        }
        return object;
    }

    public Vector<Product> getVectorProductDetail() {
        return vectorProductDetail;
    }

    public void setVectorProductDetail(Vector<Product> vectorProductDetail) {
        this.vectorProductDetail = vectorProductDetail;
    }

    
}
