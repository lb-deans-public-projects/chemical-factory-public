/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;
import entity.Customer;
import entity.Supplier;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import services.CommonService;
import services.SupplierService;
import table_models.AllCustomerTableModel;
import table_models.AllSupplierTableModel;

/**
 *
 * @author coder
 */
public class SuppliersRegistrationForm extends javax.swing.JInternalFrame {

    /**
     * Creates new form Suppliers
     */
    private SupplierService supplierService;
    private CommonService commonService;

    private Vector<Supplier> vectorSuppliers;
    private Supplier supplier;
    private AllSupplierTableModel model;

    public SuppliersRegistrationForm() {

        initComponents();

        supplierService = new SupplierService();
        commonService = new CommonService();

        vectorSuppliers = commonService.getVectorSupplier();
        model = new AllSupplierTableModel(vectorSuppliers);
        jtAllSupliers.setModel(model);

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - 30 - this.getSize().height / 2 - 20);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtAllSupliers = new javax.swing.JTable();
        btnPrintAllSuppliers = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        txtSupplierName = new javax.swing.JTextField();
        txtAddress = new javax.swing.JTextField();
        lblSupplierName = new javax.swing.JLabel();
        lblAddress2 = new javax.swing.JLabel();
        lblPhoneNumber = new javax.swing.JLabel();
        txtPhoneNumber = new javax.swing.JFormattedTextField();
        lblAddress = new javax.swing.JLabel();
        txtCnicPassport = new javax.swing.JTextField();
        btnSave = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        lblSupplierName1 = new javax.swing.JLabel();
        txtUpdateSupplierName = new javax.swing.JTextField();
        lblSupplierName2 = new javax.swing.JLabel();
        txtUpdateCnicPassport = new javax.swing.JTextField();
        lblPhoneNumber1 = new javax.swing.JLabel();
        txtUpdatePhoneNumber = new javax.swing.JFormattedTextField();
        lblAddress1 = new javax.swing.JLabel();
        txtupdateAddress = new javax.swing.JTextField();
        btnUpdate = new javax.swing.JButton();

        setClosable(true);
        setTitle("Suppliers");

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Suppliers Information", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        jtAllSupliers.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jtAllSupliers.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtAllSupliersMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jtAllSupliers);

        btnPrintAllSuppliers.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnPrintAllSuppliers.setText("Print Suppliers");
        btnPrintAllSuppliers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintAllSuppliersActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnPrintAllSuppliers, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 728, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 145, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(btnPrintAllSuppliers, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Register Supplier", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        txtSupplierName.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtSupplierName.setBorder(null);
        txtSupplierName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtSupplierNameKeyTyped(evt);
            }
        });

        txtAddress.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtAddress.setBorder(null);
        txtAddress.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddressActionPerformed(evt);
            }
        });
        txtAddress.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtAddressKeyTyped(evt);
            }
        });

        lblSupplierName.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblSupplierName.setText("Supplier Name");

        lblAddress2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblAddress2.setText("Address");

        lblPhoneNumber.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblPhoneNumber.setText("Phone Number");

        try {
            txtPhoneNumber.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####-#######")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtPhoneNumber.setText("");

        lblAddress.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblAddress.setText("CNIC Or Passport");

        txtCnicPassport.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtCnicPassport.setBorder(null);
        txtCnicPassport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCnicPassportActionPerformed(evt);
            }
        });
        txtCnicPassport.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCnicPassportKeyTyped(evt);
            }
        });

        btnSave.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnSave.setText("Save");
        btnSave.setBorder(null);
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtSupplierName, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblSupplierName, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCnicPassport, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 46, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtPhoneNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblPhoneNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblAddress2, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSave, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(lblPhoneNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtPhoneNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(lblSupplierName, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtSupplierName, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(74, 74, 74)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(lblAddress2, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(lblAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtCnicPassport, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Update Supplier", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        lblSupplierName1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblSupplierName1.setText("Supplier Name");

        txtUpdateSupplierName.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtUpdateSupplierName.setBorder(null);
        txtUpdateSupplierName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUpdateSupplierNameKeyTyped(evt);
            }
        });

        lblSupplierName2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblSupplierName2.setText("CNIC Or Passport");

        txtUpdateCnicPassport.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtUpdateCnicPassport.setBorder(null);
        txtUpdateCnicPassport.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUpdateCnicPassportKeyTyped(evt);
            }
        });

        lblPhoneNumber1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblPhoneNumber1.setText("Phone Number");

        try {
            txtUpdatePhoneNumber.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####-#######")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        lblAddress1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblAddress1.setText("Address");

        txtupdateAddress.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtupdateAddress.setBorder(null);
        txtupdateAddress.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtupdateAddressActionPerformed(evt);
            }
        });
        txtupdateAddress.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtupdateAddressKeyTyped(evt);
            }
        });

        btnUpdate.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnUpdate.setText("Update");
        btnUpdate.setBorder(null);
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblSupplierName2, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtUpdateCnicPassport, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(49, 49, 49)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblAddress1, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtupdateAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblSupplierName1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtUpdateSupplierName, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(49, 49, 49)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtUpdatePhoneNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblPhoneNumber1, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnUpdate, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(23, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPhoneNumber1, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblSupplierName1, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtUpdatePhoneNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtUpdateSupplierName, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(41, 41, 41)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSupplierName2, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblAddress1, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtupdateAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtUpdateCnicPassport, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(23, 23, 23))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(23, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(20, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtAddressActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddressActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAddressActionPerformed

    private void txtupdateAddressActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtupdateAddressActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtupdateAddressActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        String name = txtSupplierName.getText().trim();
        String phoneNumber = txtPhoneNumber.getText().trim();
        String address = txtAddress.getText().trim();
        String CnicPassport = txtCnicPassport.getText().trim();

        Supplier supplier;

        if (name.equals("") || phoneNumber.equals("") || address.equals("") || CnicPassport.equals("")) {
            JOptionPane.showMessageDialog(this, "Please fill the form properly.");
        } else {

            supplier = new Supplier();
            supplier.setSupplierName(name);
            supplier.setAddress(address);
            supplier.setPhoneNumber(phoneNumber);
            supplier.setCnicPassport(CnicPassport);

            int rowsInserted = supplierService.registerSupplier(supplier);

            if (rowsInserted == 1) {
                JOptionPane.showMessageDialog(this, "Supplier Registered  successfully.");
                txtAddress.setText("");
                txtSupplierName.setText("");
                txtPhoneNumber.setText("");
                txtCnicPassport.setText("");
                vectorSuppliers = commonService.getVectorSupplier();
                model = new AllSupplierTableModel(vectorSuppliers);
                jtAllSupliers.setModel(model);
            } else {
                JOptionPane.showMessageDialog(this, "Please try again later");
            }
        }


    }//GEN-LAST:event_btnSaveActionPerformed

    private void txtSupplierNameKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSupplierNameKeyTyped
        char txtSupplierName = evt.getKeyChar();
        if (!(Character.isAlphabetic(txtSupplierName) || txtSupplierName == KeyEvent.VK_DELETE)) {
            if (!(Character.isSpaceChar(txtSupplierName))) {
                evt.consume();
                getToolkit().beep();
            }
        }
    }//GEN-LAST:event_txtSupplierNameKeyTyped

    private void txtAddressKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAddressKeyTyped
        char txtAddress = evt.getKeyChar();
        if (!(Character.isLetterOrDigit(txtAddress) || txtAddress == KeyEvent.VK_DELETE)) {
            if (!(Character.isSpaceChar(txtAddress))) {
                evt.consume();
                getToolkit().beep();
            }
        }
    }//GEN-LAST:event_txtAddressKeyTyped

    private void txtUpdateSupplierNameKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUpdateSupplierNameKeyTyped
        char txtPhoneNumber1 = evt.getKeyChar();
        if (!(Character.isAlphabetic(txtPhoneNumber1) || txtPhoneNumber1 == KeyEvent.VK_DELETE)) {
            if (!(Character.isSpaceChar(txtPhoneNumber1))) {
                evt.consume();
                getToolkit().beep();
            }
        }
    }//GEN-LAST:event_txtUpdateSupplierNameKeyTyped

    private void txtupdateAddressKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtupdateAddressKeyTyped
        char txtAddress1 = evt.getKeyChar();
        if (!(Character.isLetterOrDigit(txtAddress1) || txtAddress1 == KeyEvent.VK_DELETE)) {
            if (!(Character.isSpaceChar(txtAddress1))) {
                evt.consume();
                getToolkit().beep();
            }
        }
    }//GEN-LAST:event_txtupdateAddressKeyTyped

    private void txtCnicPassportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCnicPassportActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCnicPassportActionPerformed

    private void txtCnicPassportKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCnicPassportKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCnicPassportKeyTyped

    private void txtUpdateCnicPassportKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUpdateCnicPassportKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtUpdateCnicPassportKeyTyped

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        int selectedRow = jtAllSupliers.getSelectedRow();

        String name = txtUpdateSupplierName.getText().trim();
        String phoneNumber = txtUpdatePhoneNumber.getText().trim();
        String address = txtupdateAddress.getText().trim();
        String cnicPassport = txtUpdateCnicPassport.getText().trim();
        model = (AllSupplierTableModel) jtAllSupliers.getModel();
        vectorSuppliers = model.getVectorSupplier();

        if (name.equals("") || phoneNumber.equals("") || address.equals("") || cnicPassport.equals("")) {
            JOptionPane.showMessageDialog(this, "Please fill the form properly.");
        } else {
            if (jtAllSupliers.isRowSelected(selectedRow)) {
                supplier = vectorSuppliers.get(selectedRow);
                supplier.setAddress(address);
                supplier.setSupplierName(name);
                supplier.setPhoneNumber(phoneNumber);
                supplier.setCnicPassport(cnicPassport);

                int rowsAffectwed = supplierService.updateSupplier(supplier);

                if (rowsAffectwed == 1) {
                    JOptionPane.showMessageDialog(this, "Supplier updated successfully.");
                    vectorSuppliers = commonService.getVectorSupplier();
                    model = new AllSupplierTableModel(vectorSuppliers);
                    jtAllSupliers.setModel(model);
                    txtUpdateSupplierName.setText("");
                    txtUpdateCnicPassport.setText("");
                    txtUpdatePhoneNumber.setText("");
                    txtupdateAddress.setText("");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Please select supplier from table that you want to update");
            }

        }
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void jtAllSupliersMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtAllSupliersMouseClicked
        int selectedRow = jtAllSupliers.getSelectedRow();
        model = (AllSupplierTableModel) jtAllSupliers.getModel();
        vectorSuppliers = model.getVectorSupplier();
        supplier = vectorSuppliers.get(selectedRow);

        txtUpdateSupplierName.setText(supplier.getSupplierName());
        txtupdateAddress.setText(supplier.getAddress());

        txtUpdatePhoneNumber.setText(supplier.getPhoneNumber());
        txtUpdateCnicPassport.setText(supplier.getCnicPassport());


    }//GEN-LAST:event_jtAllSupliersMouseClicked

    private void btnPrintAllSuppliersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintAllSuppliersActionPerformed
        try {
            PageSize size = new PageSize(PageSize.A4);
            PdfDocument pdfDocument = new PdfDocument(new PdfWriter("src/reports/All_Suppliers.pdf"));
            Document layoutDocument = new Document(pdfDocument, size);
            layoutDocument.add(new Paragraph("All_Suppliers").setBold().setUnderline().setBackgroundColor(Color.BLUE).setTextAlignment(TextAlignment.CENTER));
            Table table = new Table(UnitValue.createPointArray(new float[]{30f, 150f, 120f, 120f, 100f}));
            table.setTextAlignment(TextAlignment.CENTER);
            table.addHeaderCell(new Paragraph("S #"));
            table.addHeaderCell(new Paragraph("Name"));
            table.addHeaderCell(new Paragraph("CNIC"));
            table.addHeaderCell(new Paragraph("Address"));
            table.addHeaderCell(new Paragraph("Phone Number"));
            int sNo = 0;
            for (Supplier supplier : vectorSuppliers) {
                sNo = sNo + 1;
                table.addCell(new Paragraph((sNo + "")));
                table.addCell(new Paragraph(supplier.getSupplierName()));

                table.addCell(new Paragraph(supplier.getAddress()));
                table.addCell(new Paragraph(supplier.getPhoneNumber()));
                table.addCell(new Paragraph(supplier.getCnicPassport()));
            }
            layoutDocument.add(table);
            layoutDocument.close();
//
            File pdfFile = new File("src/reports/All_Suppliers.pdf");
            if (pdfFile.exists()) {
                if (Desktop.isDesktopSupported()) {
                    Desktop.getDesktop().open(pdfFile);
                    System.out.println("File has been opened successfully.");
                } else {
                    System.out.println("Awt Desktop is not supported!");
                }

            } else {
                System.out.println("File is not exists!");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }//GEN-LAST:event_btnPrintAllSuppliersActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnPrintAllSuppliers;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jtAllSupliers;
    private javax.swing.JLabel lblAddress;
    private javax.swing.JLabel lblAddress1;
    private javax.swing.JLabel lblAddress2;
    private javax.swing.JLabel lblPhoneNumber;
    private javax.swing.JLabel lblPhoneNumber1;
    private javax.swing.JLabel lblSupplierName;
    private javax.swing.JLabel lblSupplierName1;
    private javax.swing.JLabel lblSupplierName2;
    private javax.swing.JTextField txtAddress;
    private javax.swing.JTextField txtCnicPassport;
    private javax.swing.JFormattedTextField txtPhoneNumber;
    private javax.swing.JTextField txtSupplierName;
    private javax.swing.JTextField txtUpdateCnicPassport;
    private javax.swing.JFormattedTextField txtUpdatePhoneNumber;
    private javax.swing.JTextField txtUpdateSupplierName;
    private javax.swing.JTextField txtupdateAddress;
    // End of variables declaration//GEN-END:variables
}
