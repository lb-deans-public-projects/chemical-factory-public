/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Div;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;
import com.sun.glass.events.KeyEvent;
import entity.Customer;
import entity.CustomerPayment;
import entity.EmployeeSalary;
import entity.PaymentMethod;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import services.CommonService;
import services.CustomerServices;
import table_models.CustomerPaymentHistoryTableModel;

/**
 *
 * @author Net Valley
 */
public class CustomerPaymentForm extends javax.swing.JInternalFrame {

    private CommonService commonService;
    private Vector<Customer> vectorCustomer;
    private CustomerServices customerService;
    private Vector<CustomerPayment> vectorCustomerPaymentHistory;
    private CustomerPaymentHistoryTableModel tableModel;

    public CustomerPaymentForm() {
        initComponents();

        commonService = new CommonService();
        vectorCustomer = commonService.getVectorCustomer();
        customerService = new CustomerServices();

        DefaultComboBoxModel model = new DefaultComboBoxModel(vectorCustomer);
        cmbCustomer.setModel(model);
        cmbSelectCustomer.setModel(model);
        cmbPaymentMethod.setModel(new DefaultComboBoxModel(commonService.getVectorPaymentMethod()));

//        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
//        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - 30 - this.getSize().height / 2 - 20);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        txtSave = new javax.swing.JButton();
        lbCustomer = new javax.swing.JLabel();
        cmbCustomer = new javax.swing.JComboBox<String>();
        lbDate = new javax.swing.JLabel();
        lbBalance = new javax.swing.JLabel();
        txtBalance = new javax.swing.JTextField();
        lbAmountPay = new javax.swing.JLabel();
        txtPaidAmount = new javax.swing.JTextField();
        jdTransactionDate = new com.toedter.calendar.JDateChooser();
        lblTransactionDate1 = new javax.swing.JLabel();
        cmbPaymentMethod = new javax.swing.JComboBox<String>();
        jLabel2 = new javax.swing.JLabel();
        txtChequeNumber = new javax.swing.JTextField();
        txtBankName = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtBranchName = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtCustomerPaymentHistory = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        cmbSelectCustomer = new javax.swing.JComboBox();
        btnGeneratePDF = new javax.swing.JButton();

        setClosable(true);
        setTitle("Customer Payment");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Payment Detail", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        txtSave.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtSave.setText("Save");
        txtSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSaveActionPerformed(evt);
            }
        });

        lbCustomer.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lbCustomer.setText("Select Customer");

        cmbCustomer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbCustomerActionPerformed(evt);
            }
        });

        lbDate.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lbDate.setText("Select Date");

        lbBalance.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lbBalance.setText("Balance");

        txtBalance.setEditable(false);
        txtBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBalanceActionPerformed(evt);
            }
        });

        lbAmountPay.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lbAmountPay.setText("Amount to Pay");

        txtPaidAmount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPaidAmountKeyTyped(evt);
            }
        });

        lblTransactionDate1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblTransactionDate1.setText("Payment Method");

        cmbPaymentMethod.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cmbPaymentMethod.setBorder(null);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Cheque Number");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Bank Name");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setText("Branch Name");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lbCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmbCustomer, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(lbDate, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jdTransactionDate, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtChequeNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtSave, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(71, 71, 71)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(lbBalance, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtBalance, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtPaidAmount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lbAmountPay, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txtBankName, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(67, 67, 67)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(cmbPaymentMethod, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblTransactionDate1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txtBranchName, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(31, 31, 31))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lbCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cmbCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(27, 27, 27)
                                .addComponent(lbDate, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jdTransactionDate, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblTransactionDate1, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cmbPaymentMethod, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(26, 26, 26)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtChequeNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtBranchName, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lbBalance, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtBalance, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(85, 85, 85))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lbAmountPay, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtPaidAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(26, 26, 26)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBankName, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(30, 30, 30)
                .addComponent(txtSave, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(30, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Payment History", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        jtCustomerPaymentHistory.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jtCustomerPaymentHistory);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Select Customer");

        cmbSelectCustomer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbSelectCustomerActionPerformed(evt);
            }
        });

        btnGeneratePDF.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnGeneratePDF.setText("Generate PDF");
        btnGeneratePDF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGeneratePDFActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(72, 72, 72)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(cmbSelectCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(105, 105, 105)
                        .addComponent(btnGeneratePDF, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 572, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(cmbSelectCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnGeneratePDF, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(32, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSaveActionPerformed

        Customer customer = (Customer) cmbCustomer.getSelectedItem();
        String balance = txtBalance.getText().trim();
        Date date = jdTransactionDate.getDate();
        String paidAmount = txtPaidAmount.getText();
        PaymentMethod paymentMethod = (PaymentMethod) cmbPaymentMethod.getSelectedItem();
        String selectedMethod = paymentMethod.getType();
        String chequeNumber = txtChequeNumber.getText().trim();
        String bankName = txtBankName.getText().trim();
        String branchName = txtBranchName.getText().trim();
        if (balance.equals("") || date == null) {
            JOptionPane.showMessageDialog(this, "Please select customer and date");
            txtPaidAmount.setText("");
        } else {
            if (paidAmount.equals("")) {
                JOptionPane.showMessageDialog(this, "Please enter paid Amount..");
            } else {
                if (selectedMethod.equals("Cheque")) {
                    if (chequeNumber.equals("") || bankName.equals("") || branchName.equals("")) {
                        JOptionPane.showMessageDialog(this, "Please enter detail about cheque");
                    } else {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        String transactionDate = sdf.format(date);
                        CustomerPayment customerPayment = new CustomerPayment();
                        customerPayment.setCustomer(customer);
                        customerPayment.setCustomerBalance(Integer.parseInt(balance));
                        customerPayment.setDate(transactionDate);
                        customerPayment.setCustomerAmountPay(Integer.parseInt(paidAmount));
                        paymentMethod.setChequeNumber(chequeNumber);
                        paymentMethod.setBankName(bankName);
                        paymentMethod.setBranchName(branchName);
                        customerPayment.setPaymentMethod(paymentMethod);
                        int rowsAffected = customerService.enterCustomerPayment(customerPayment);
                        if (rowsAffected > 0) {
                            JOptionPane.showMessageDialog(this, "Customer payment information inserted successfully");
                            txtPaidAmount.setText("");
                            txtChequeNumber.setText("");
                            txtBankName.setText("");
                            txtBranchName.setText("");
                            int newBalance = commonService.getCustomerRemaingBalance(customer);
                            txtBalance.setText(newBalance + "");
                        }
                    }
                } else {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    String transactionDate = sdf.format(date);
                    CustomerPayment customerPayment = new CustomerPayment();
                    customerPayment.setCustomer(customer);
                    customerPayment.setCustomerBalance(Integer.parseInt(balance));
                    customerPayment.setDate(transactionDate);
                    customerPayment.setCustomerAmountPay(Integer.parseInt(paidAmount));
                    customerPayment.setPaymentMethod(paymentMethod);
                    int rowsAffected = customerService.enterCustomerPayment(customerPayment);
                    if (rowsAffected > 0) {
                        JOptionPane.showMessageDialog(this, "Customer payment information inserted successfully");
                        txtPaidAmount.setText("");
                        txtChequeNumber.setText("");
                        txtBankName.setText("");
                        txtBranchName.setText("");
                        int newBalance = commonService.getCustomerRemaingBalance(customer);
                        txtBalance.setText(newBalance + "");
                    }
                }
            }
        }

    }//GEN-LAST:event_txtSaveActionPerformed

    private void cmbCustomerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbCustomerActionPerformed

        Customer customer = (Customer) cmbCustomer.getSelectedItem();
        int balance = commonService.getCustomerRemaingBalance(customer);

        txtBalance.setText(balance + "");

    }//GEN-LAST:event_cmbCustomerActionPerformed

    private void txtBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBalanceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtBalanceActionPerformed

    private void txtPaidAmountKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPaidAmountKeyTyped
        char txtAmountPay = evt.getKeyChar();
        if (!(Character.isDigit(txtAmountPay) || txtAmountPay == KeyEvent.VK_BACKSPACE || txtAmountPay == KeyEvent.VK_DELETE)) {
            evt.consume();
            getToolkit().beep();

        }
    }//GEN-LAST:event_txtPaidAmountKeyTyped

    private void cmbSelectCustomerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbSelectCustomerActionPerformed

        Customer customer = (Customer) cmbSelectCustomer.getSelectedItem();
        vectorCustomerPaymentHistory = customerService.getVectorCustomerPaymentHistory(customer);
        tableModel = new CustomerPaymentHistoryTableModel(vectorCustomerPaymentHistory);
        jtCustomerPaymentHistory.setModel(tableModel);
    }//GEN-LAST:event_cmbSelectCustomerActionPerformed

    private void btnGeneratePDFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGeneratePDFActionPerformed
        Customer customer = (Customer) cmbSelectCustomer.getSelectedItem();
        if(tableModel == null) {
            JOptionPane.showMessageDialog(this, "Please select employee from payment history combo box");
        } else {
            
            try {
            PageSize size = new PageSize(PageSize.LETTER);
            PdfDocument pdfDocument = new PdfDocument(new PdfWriter("src/reports/customer_payment.pdf"));
            Document layoutDocument = new Document(pdfDocument, size);
            layoutDocument.setMargins(10, 20, 10, 20);

            layoutDocument.add(new Paragraph("Customer Payment History").setBold().setTextAlignment(TextAlignment.CENTER).setFontSize(14).setUnderline());
            layoutDocument.add(new Paragraph(customer.getName()).setFontSize(14));
            layoutDocument.add(new Paragraph(customer.getAddress()).setFontSize(14));
            layoutDocument.add(new Paragraph(customer.getPhoneNumber()).setFontSize(14));

            Table table = new Table(UnitValue.createPointArray(new float[]{40f, 80f, 120f, 130f, 100f, 100f, 130f}));

            table.addCell(new Paragraph("S.NO").setBold().setFontColor(com.itextpdf.kernel.color.Color.BLACK));
            table.addCell(new Paragraph("Date").setBold().setFontColor(com.itextpdf.kernel.color.Color.BLACK));
            table.addCell(new Paragraph("Paid Amount").setBold().setFontColor(com.itextpdf.kernel.color.Color.BLACK));
            table.addCell(new Paragraph("Payment Method").setBold().setFontColor(com.itextpdf.kernel.color.Color.BLACK));
            table.addCell(new Paragraph("Cheque#").setBold().setFontColor(com.itextpdf.kernel.color.Color.BLACK));
            table.addCell(new Paragraph("Bank Name").setBold().setFontColor(com.itextpdf.kernel.color.Color.BLACK));
            table.addCell(new Paragraph("Branch Name").setBold().setFontColor(com.itextpdf.kernel.color.Color.BLACK));

            int total = 0;
            for (CustomerPayment customerPayment : vectorCustomerPaymentHistory) {
                table.addCell(new Paragraph(customerPayment.getSerialNumber() + ""));
                table.addCell(new Paragraph(customerPayment.getDate()));
                table.addCell(new Paragraph(customerPayment.getCustomerAmountPay() +""));
                table.addCell(new Paragraph(customerPayment.getPaymentMethod().getType()));
                table.addCell(new Paragraph(customerPayment.getPaymentMethod().getChequeNumber()));
                table.addCell(new Paragraph(customerPayment.getPaymentMethod().getBankName()));
                table.addCell(new Paragraph(customerPayment.getPaymentMethod().getBranchName()));
                total = total + customerPayment.getCustomerAmountPay();

            }
            table.setMarginTop(10);
            table.setTextAlignment(TextAlignment.CENTER);
            layoutDocument.add(table);
            layoutDocument.add(new Paragraph("Total: "+ total).setFontSize(14).setTextAlignment(TextAlignment.RIGHT).setMarginRight(5));
            layoutDocument.close();

            File pdfFile = new File("src/reports/customer_payment.pdf");

            if (pdfFile.exists()) {
                if (Desktop.isDesktopSupported()) {
                    Desktop.getDesktop().open(pdfFile);
                } else {
                    System.out.println("Awt Dose not supported");
                }
            } else {
                System.out.println("File does not exist");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        }
    }//GEN-LAST:event_btnGeneratePDFActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGeneratePDF;
    private javax.swing.JComboBox<String> cmbCustomer;
    private javax.swing.JComboBox<String> cmbPaymentMethod;
    private javax.swing.JComboBox cmbSelectCustomer;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private com.toedter.calendar.JDateChooser jdTransactionDate;
    private javax.swing.JTable jtCustomerPaymentHistory;
    private javax.swing.JLabel lbAmountPay;
    private javax.swing.JLabel lbBalance;
    private javax.swing.JLabel lbCustomer;
    private javax.swing.JLabel lbDate;
    private javax.swing.JLabel lblTransactionDate1;
    private javax.swing.JTextField txtBalance;
    private javax.swing.JTextField txtBankName;
    private javax.swing.JTextField txtBranchName;
    private javax.swing.JTextField txtChequeNumber;
    private javax.swing.JTextField txtPaidAmount;
    private javax.swing.JButton txtSave;
    // End of variables declaration//GEN-END:variables
}
