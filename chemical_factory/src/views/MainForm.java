/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

/**
 *
 * @author coder
 */
public class MainForm extends javax.swing.JFrame {

    private ProductCostForm productCostForm;
    private EmployeeAbsenteeForm employeeAbsenteeForm;
    private EmployeeAdvanceForm employeeAdvanceForm;
    private EmployeeRegistrationForm employeeRegistrationForm;
    private EmplyoeeOverTimeForm emplyoeeOverTimeForm;
    private EmployeeSalaryHistoryForm employeeSalaryHistoryForm;
    private CustomerRegistrationForm customerForm;
    private DailySalesForm dailySalesForm;
    private CustomerPaymentForm customerPaymentForm;
    private SuppliersRegistrationForm suppliersRegistrationForm;
    private ItemsForm itemsForm;
    private PurchasesForm purchasesForm;
    private CreditPurchaseHistoryForm creditPurchaseHistoryForm;
    private UserRegistrationForm userRegistrationForm;
    private ProductAddForm productAddForm;
    private ProductDetailsForm productDetailsForm;
    

    private String wellcomeMessage;

    public MainForm() {
        initComponents();
        this.setExtendedState(MAXIMIZED_BOTH);
    }

    public MainForm(String displayName) {
        initComponents();

        this.setExtendedState(MAXIMIZED_BOTH);

        wellcomeMessage = "Welcome "+displayName + " !";
        
        this.lblWellcomeMessage.setText(wellcomeMessage);
    }

    public void closedOtherForm() {

        //Register employee form dispose method
        if (employeeRegistrationForm != null && employeeRegistrationForm.isClosed() == false) {
            employeeRegistrationForm.dispose();
        }
        //All Employees form dispose method 
       
        //employee advance form dispose method
        if (employeeAdvanceForm != null && employeeAdvanceForm.isClosed() == false) {
            employeeAdvanceForm.dispose();
        }
        //employee absentee form dispose method
        if (employeeAbsenteeForm != null && employeeAbsenteeForm.isClosed() == false) {
            employeeAbsenteeForm.dispose();
        }
        // employee overTime form dispose method
        if (emplyoeeOverTimeForm != null && emplyoeeOverTimeForm.isClosed() == false) {
            emplyoeeOverTimeForm.dispose();
        }
        //employee Salary history form dispose method
        if (employeeSalaryHistoryForm != null && employeeSalaryHistoryForm.isClosed() == false) {
            employeeSalaryHistoryForm.dispose();
        }
        //customer form dispose method
        if (customerForm != null && customerForm.isClosed() == false) {
            customerForm.dispose();
        }
        //daily sales form dispose method
        if (dailySalesForm != null && dailySalesForm.isClosed() == false) {
            dailySalesForm.dispose();
        }
        // customer payment form dispose method
        if (customerPaymentForm != null && customerPaymentForm.isClosed() == false) {
            customerPaymentForm.dispose();
        }
        // supplier registration form dispose method 
        if (suppliersRegistrationForm != null && suppliersRegistrationForm.isClosed() == false) {
            suppliersRegistrationForm.dispose();
        }
        //items form dispose method
        if (itemsForm != null && itemsForm.isClosed() == false) {
            itemsForm.dispose();
        }
        //purchases form dispose method
        if (purchasesForm != null && purchasesForm.isClosed() == false) {
            purchasesForm.dispose();
        }
        //credit purchases history form dispose method
        if (creditPurchaseHistoryForm != null && creditPurchaseHistoryForm.isClosed() == false) {
            creditPurchaseHistoryForm.dispose();
        }
        // user registration form dispose method
        if (userRegistrationForm != null && userRegistrationForm.isClosed() == false) {
            userRegistrationForm.dispose();
        }
        //product add form dispose method
        if (productAddForm != null && productAddForm.isClosed() == false) {
            productAddForm.dispose();
        }
        //product details form dispose method
        if (productDetailsForm != null && productDetailsForm.isClosed() == false) {
            productDetailsForm.dispose();
        }
        //product cost form dispose method 
        if (productCostForm != null && productCostForm.isClosed() == false) {
            productCostForm.dispose();
        }
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblWellcomeMessage = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        mIRegisterEmployee = new javax.swing.JMenuItem();
        mIAdvance = new javax.swing.JMenuItem();
        mIAbsentee = new javax.swing.JMenuItem();
        mIOverTime = new javax.swing.JMenuItem();
        mIPaySalary = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        mIRegisterCustomer = new javax.swing.JMenuItem();
        mIDailySales = new javax.swing.JMenuItem();
        mIPayments = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        mIRegisterSupplier = new javax.swing.JMenuItem();
        mIItems = new javax.swing.JMenuItem();
        mIPurchases = new javax.swing.JMenuItem();
        mICrditPurchaseHistory = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        mIAddUser = new javax.swing.JMenuItem();
        mIAddProducts = new javax.swing.JMenuItem();
        mIAddProductDetails = new javax.swing.JMenuItem();
        mIProductCost = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        lblWellcomeMessage.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        lblWellcomeMessage.setForeground(new java.awt.Color(51, 255, 51));
        lblWellcomeMessage.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lblWellcomeMessage.setEnabled(false);

        jMenu1.setText("Employee");

        mIRegisterEmployee.setText("Register Employee");
        mIRegisterEmployee.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mIRegisterEmployeeActionPerformed(evt);
            }
        });
        jMenu1.add(mIRegisterEmployee);

        mIAdvance.setText("Advance");
        mIAdvance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mIAdvanceActionPerformed(evt);
            }
        });
        jMenu1.add(mIAdvance);

        mIAbsentee.setText("Absentee");
        mIAbsentee.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mIAbsenteeActionPerformed(evt);
            }
        });
        jMenu1.add(mIAbsentee);

        mIOverTime.setText("OverTime");
        mIOverTime.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mIOverTimeActionPerformed(evt);
            }
        });
        jMenu1.add(mIOverTime);

        mIPaySalary.setText("Pay Salary Employee");
        mIPaySalary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mIPaySalaryActionPerformed(evt);
            }
        });
        jMenu1.add(mIPaySalary);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Customer");
        jMenu2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu2ActionPerformed(evt);
            }
        });

        mIRegisterCustomer.setText("Register Customer");
        mIRegisterCustomer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mIRegisterCustomerActionPerformed(evt);
            }
        });
        jMenu2.add(mIRegisterCustomer);

        mIDailySales.setText("Daily Sales");
        mIDailySales.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mIDailySalesActionPerformed(evt);
            }
        });
        jMenu2.add(mIDailySales);

        mIPayments.setText("Payments");
        mIPayments.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mIPaymentsActionPerformed(evt);
            }
        });
        jMenu2.add(mIPayments);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("Supplier");
        jMenu3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu3ActionPerformed(evt);
            }
        });

        mIRegisterSupplier.setText("Register Supplier");
        mIRegisterSupplier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mIRegisterSupplierActionPerformed(evt);
            }
        });
        jMenu3.add(mIRegisterSupplier);

        mIItems.setText("Items");
        mIItems.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mIItemsActionPerformed(evt);
            }
        });
        jMenu3.add(mIItems);

        mIPurchases.setText("Purchases");
        mIPurchases.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mIPurchasesActionPerformed(evt);
            }
        });
        jMenu3.add(mIPurchases);

        mICrditPurchaseHistory.setText("Payment To Supplier");
        mICrditPurchaseHistory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mICrditPurchaseHistoryActionPerformed(evt);
            }
        });
        jMenu3.add(mICrditPurchaseHistory);

        jMenuBar1.add(jMenu3);

        jMenu4.setText("System Wide Setting");

        mIAddUser.setText("Add User");
        mIAddUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mIAddUserActionPerformed(evt);
            }
        });
        jMenu4.add(mIAddUser);

        mIAddProducts.setText("Add Products");
        mIAddProducts.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mIAddProductsActionPerformed(evt);
            }
        });
        jMenu4.add(mIAddProducts);

        mIAddProductDetails.setText("Add Product Details");
        mIAddProductDetails.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mIAddProductDetailsActionPerformed(evt);
            }
        });
        jMenu4.add(mIAddProductDetails);

        mIProductCost.setText("Product Cost / Production  / Add To Stock");
        mIProductCost.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mIProductCostActionPerformed(evt);
            }
        });
        jMenu4.add(mIProductCost);

        jMenuBar1.add(jMenu4);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(414, Short.MAX_VALUE)
                .addComponent(lblWellcomeMessage, javax.swing.GroupLayout.PREFERRED_SIZE, 371, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(lblWellcomeMessage, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 449, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mIProductCostActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mIProductCostActionPerformed
        closedOtherForm();
        productCostForm = new ProductCostForm();
        this.add(productCostForm);
        productCostForm.setVisible(true);
    }//GEN-LAST:event_mIProductCostActionPerformed

    private void mIRegisterEmployeeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mIRegisterEmployeeActionPerformed
        closedOtherForm();
        employeeRegistrationForm = new EmployeeRegistrationForm();
        this.add(employeeRegistrationForm);
        employeeRegistrationForm.setVisible(true);
    }//GEN-LAST:event_mIRegisterEmployeeActionPerformed

    private void mIAdvanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mIAdvanceActionPerformed
        closedOtherForm();
        employeeAdvanceForm = new EmployeeAdvanceForm();
        this.add(employeeAdvanceForm);
        employeeAdvanceForm.setVisible(true);
    }//GEN-LAST:event_mIAdvanceActionPerformed

    private void mIAbsenteeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mIAbsenteeActionPerformed
        closedOtherForm();
        employeeAbsenteeForm = new EmployeeAbsenteeForm();
        this.add(employeeAbsenteeForm);
        employeeAbsenteeForm.setVisible(true);
    }//GEN-LAST:event_mIAbsenteeActionPerformed

    private void mIOverTimeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mIOverTimeActionPerformed
        closedOtherForm();
        emplyoeeOverTimeForm = new EmplyoeeOverTimeForm();
        this.add(emplyoeeOverTimeForm);
        emplyoeeOverTimeForm.setVisible(true);
    }//GEN-LAST:event_mIOverTimeActionPerformed

    private void mIPaySalaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mIPaySalaryActionPerformed
        closedOtherForm();
        employeeSalaryHistoryForm = new EmployeeSalaryHistoryForm();
        this.add(employeeSalaryHistoryForm);
        employeeSalaryHistoryForm.setVisible(true);
    }//GEN-LAST:event_mIPaySalaryActionPerformed

    private void mIRegisterCustomerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mIRegisterCustomerActionPerformed
        closedOtherForm();
        customerForm = new CustomerRegistrationForm();
        this.add(customerForm);
        customerForm.setVisible(true);
    }//GEN-LAST:event_mIRegisterCustomerActionPerformed

    private void mIDailySalesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mIDailySalesActionPerformed
        closedOtherForm();
        dailySalesForm = new DailySalesForm();
        this.add(dailySalesForm);
        dailySalesForm.setVisible(true);
    }//GEN-LAST:event_mIDailySalesActionPerformed

    private void mIPaymentsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mIPaymentsActionPerformed
        closedOtherForm();
        customerPaymentForm = new CustomerPaymentForm();
        this.add(customerPaymentForm);
        customerPaymentForm.setVisible(true);
    }//GEN-LAST:event_mIPaymentsActionPerformed

    private void mIRegisterSupplierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mIRegisterSupplierActionPerformed
        closedOtherForm();
        suppliersRegistrationForm = new SuppliersRegistrationForm();
        this.add(suppliersRegistrationForm);
        suppliersRegistrationForm.setVisible(true);
    }//GEN-LAST:event_mIRegisterSupplierActionPerformed

    private void mIItemsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mIItemsActionPerformed
        closedOtherForm();
        itemsForm = new ItemsForm();
        this.add(itemsForm);
        itemsForm.setVisible(true);
    }//GEN-LAST:event_mIItemsActionPerformed

    private void mIPurchasesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mIPurchasesActionPerformed
        closedOtherForm();
        purchasesForm = new PurchasesForm();
        this.add(purchasesForm);
        purchasesForm.setVisible(true);
    }//GEN-LAST:event_mIPurchasesActionPerformed

    private void mICrditPurchaseHistoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mICrditPurchaseHistoryActionPerformed
        closedOtherForm();
        creditPurchaseHistoryForm = new CreditPurchaseHistoryForm();
        this.add(creditPurchaseHistoryForm);
        creditPurchaseHistoryForm.setVisible(true);
    }//GEN-LAST:event_mICrditPurchaseHistoryActionPerformed

    private void mIAddUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mIAddUserActionPerformed
        closedOtherForm();
        userRegistrationForm = new UserRegistrationForm();
        this.add(userRegistrationForm);
        userRegistrationForm.setVisible(true);
    }//GEN-LAST:event_mIAddUserActionPerformed

    private void mIAddProductsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mIAddProductsActionPerformed
        closedOtherForm();
        productAddForm = new ProductAddForm();
        this.add(productAddForm);
        productAddForm.setVisible(true);
    }//GEN-LAST:event_mIAddProductsActionPerformed

    private void mIAddProductDetailsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mIAddProductDetailsActionPerformed
        closedOtherForm();
        productDetailsForm = new ProductDetailsForm();
        this.add(productDetailsForm);
        productDetailsForm.setVisible(true);
    }//GEN-LAST:event_mIAddProductDetailsActionPerformed

    private void jMenu2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenu2ActionPerformed

    private void jMenu3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenu3ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JLabel lblWellcomeMessage;
    private javax.swing.JMenuItem mIAbsentee;
    private javax.swing.JMenuItem mIAddProductDetails;
    private javax.swing.JMenuItem mIAddProducts;
    private javax.swing.JMenuItem mIAddUser;
    private javax.swing.JMenuItem mIAdvance;
    private javax.swing.JMenuItem mICrditPurchaseHistory;
    private javax.swing.JMenuItem mIDailySales;
    private javax.swing.JMenuItem mIItems;
    private javax.swing.JMenuItem mIOverTime;
    private javax.swing.JMenuItem mIPaySalary;
    private javax.swing.JMenuItem mIPayments;
    private javax.swing.JMenuItem mIProductCost;
    private javax.swing.JMenuItem mIPurchases;
    private javax.swing.JMenuItem mIRegisterCustomer;
    private javax.swing.JMenuItem mIRegisterEmployee;
    private javax.swing.JMenuItem mIRegisterSupplier;
    // End of variables declaration//GEN-END:variables
}
