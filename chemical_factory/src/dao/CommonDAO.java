package dao;

import entity.Customer;
import entity.Item;
import entity.PaymentMethod;
import entity.Product;
import entity.Supplier;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import util.SQLQueryUtil;

/**
 *
 * @author rajaa
 */
public class CommonDAO {
    
    
    public Vector<Product> getVectorProductCostDetail(){
        Vector<Product> vectorProductCostDetail = new Vector<>();
        String query = "SELECT `p`.`product_name`, `pd`.`product_color`, `pd`.`product_size`, "
                + "DATE_FORMAT(`pc`.`insertion_date_time`, '%d %b %y') AS `insertion_date_time`, "
                + "`pc`.`total_expense`, `pp`.`quantity_produced`, "
                + "`pp`.`cost_price` \n FROM `products` AS `p`, `product_details` AS `pd`, "
                + "`product_cost` AS `pc` , `products_produced` AS `pp`\n" +
                    "WHERE `p`.`id` = `pd`.`products_id` AND\n" +
                    "`pd`.`id` = `pc`.`product_details_id` AND\n" +
                    "`pc`.`id` = `pp`.`product_cost_id`"
                + "ORDER BY `pc`.`insertion_date_time` ASC;";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        Product product;
        try {
            resultSet = sql.executeQuery(query);
            int serialNumber = 0;
            while (resultSet.next()) {
                serialNumber = serialNumber + 1;
                String productName = resultSet.getString("product_name");
                String productColor = resultSet.getString("product_color");
                String productSize = resultSet.getString("product_size");
                String date = resultSet.getString("insertion_date_time");
                int totalExpense = resultSet.getInt("total_expense");
                int quantity = resultSet.getInt("quantity_produced");
                int costPrice = resultSet.getInt("cost_price");
                
                product = new Product();
                product.setSerialNumber(serialNumber);
                product.setProductName(productName);
                product.setColor(productColor);
                product.setSize(productSize);
                product.setData(date);
                product.setTotalExpense(totalExpense);
                product.setQuantity(quantity);
                product.setPricePerKg(costPrice);
                
                vectorProductCostDetail.add(product);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return vectorProductCostDetail;
    }
    
    public Vector<Item> getVectorItems(Supplier supplier) {
        Vector<Item> vectorItems = new Vector<>();
        String query = "SELECT * FROM `raw_material_items` WHERE `suppliers_id` = "
                + supplier.getSupplierId() + " ORDER BY `item_name` ASC;";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        Item item;
        try {
            resultSet = sql.executeQuery(query);
            int serialNumber = 1;
            while (resultSet.next()) {
                item = new Item();
                item.setItemId(resultSet.getInt("id"));
                item.setSerialNumber(serialNumber);
                item.setSupplier(supplier);
                item.setItemName(resultSet.getString("item_name"));
                vectorItems.add(item);
                serialNumber++;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return vectorItems;
    }
    
    public Vector<PaymentMethod> getVectorPaymentMethod() {
        Vector<PaymentMethod> vectorPaymentMethod = new Vector<>();
        String query = "SELECT * FROM `payment_methods`";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        PaymentMethod paymentMethod;
        try {
            resultSet = sql.executeQuery(query);
            while (resultSet.next()) {
                paymentMethod = new PaymentMethod(resultSet.getString("id"), resultSet.getString("type"));
                vectorPaymentMethod.add(paymentMethod);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return vectorPaymentMethod;
    }
    
    public int getCustomerRemaingBalance(Customer customer) {
        String query = "SELECT IFNULL ( SUM(`cr_amount` -`dr_amount`), 0) AS `total_balance` "
                + "FROM `customer_credit_history` WHERE `customer_id` = "
                + customer.getcustomerId() + ";";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        int totalBalance = 0;
        try {
            resultSet = sql.executeQuery(query);
            resultSet.next();
            totalBalance = resultSet.getInt("total_balance");
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return totalBalance;
    }
}
