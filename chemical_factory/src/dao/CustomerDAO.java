/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.Customer;
import entity.CustomerPayment;
import entity.PaymentMethod;
import entity.Product;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import javax.swing.JOptionPane;
import util.SQLQueryUtil;

/**
 *
 * @author Haroon Afridi
 */
public class CustomerDAO {

    public int registerCustomer(Customer customer) {
        String query = "";
        int rowsAffected = 0;
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        try {
            query = "INSERT INTO `customer`(`customer_name`, `address`, `phone_number`, `cnic`)"
                    + " VALUES ('" + customer.getName() + "','" + customer.getAddress() + "',"
                    + "'" + customer.getPhoneNumber() + "','" + customer.getCnic() + "');";

            rowsAffected = sql.executeUpdate(query);
            sql.commit();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return rowsAffected;

    }

    public int updateCustomer(Customer customer) {
        String query = "";
        int rowsAffected = 0;
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);

        try {
            query = "UPDATE `customer` SET `customer_name` = '"
                    + customer.getName() + "', `address` = '"
                    + customer.getAddress() + "', `phone_number` = '"
                    + customer.getPhoneNumber() + "', `cnic` = '"
                    + customer.getCnic() + "' \n"
                    + "WHERE `id` = "
                    + customer.getcustomerId() + ";";
            rowsAffected = sql.executeUpdate(query);
            sql.commit();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return rowsAffected;
    }

    public int enterDailySales(Vector<Product> vectorProductSales, Product product) {
        String queryCustomerVisit = "INSERT INTO `customer_visit` (`customer_id`, "
                + "`total_bill`, `received_amount`, `balance`, `transaction_date`, "
                + "`insertion_date_time`, `payment_methods_id`, `cheque_number`, "
                + "`bank_name`, `branch_name`) VALUES ("
                + vectorProductSales.get(0).getCustomer().getcustomerId() + ", "
                + product.getGrandTotal() + ", "
                + product.getReceivedAmount() + ", "
                + (product.getGrandTotal() - product.getReceivedAmount()) + ", '"
                + vectorProductSales.get(0).getData() + "', `getCurrentDateTime`(), '"
                + product.getPaymentMethod().getId() + "', '"
                + product.getPaymentMethod().getChequeNumber() + "', '"
                + product.getPaymentMethod().getBankName() + "', '"
                + product.getPaymentMethod().getBranchName() + "');";

        String queryMaxId = "SELECT IFNULL(MAX(`id`), 0) AS `max_id` FROM `customer_visit`;";
        String queryCustomerVisitHistory = "";
        String queryCustomerCreditHistoryOne = "";
        String queryCustomerCreditHistoryTwo = "";
        String queryStock = "";
        int maxId;
        ResultSet resultSet;
        int rowsAffected = 0;
        int rowsAffectedOne = 0;
        int rowsAffectedTwo = 0;
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);

        try {
            rowsAffectedOne = sql.executeUpdate(queryCustomerVisit);
            if (rowsAffectedOne > 0) {
                resultSet = sql.executeQuery(queryMaxId);
                resultSet.next();
                maxId = resultSet.getInt("max_id");
                int i;
                for (i = 0; i < vectorProductSales.size(); i++) {
                    queryCustomerVisitHistory = "INSERT INTO `customer_visit_history` ("
                            + "`customer_visit_id`, `product_details_id`, `unit_price`, "
                            + "`quantity`) VALUES ("
                            + maxId + ", "
                            + vectorProductSales.get(i).getProductColor().getProductDetailId() + ", "
                            + vectorProductSales.get(i).getPricePerKg() + ", "
                            + vectorProductSales.get(i).getQuantity() + ");";
                    sql.executeUpdate(queryCustomerVisitHistory);

                    queryStock = "UPDATE `stock` SET `quantity` = `quantity` - "
                            + vectorProductSales.get(i).getQuantity() + " WHERE `product_details_id` = "
                            + vectorProductSales.get(i).getProductColor().getProductDetailId() + ";";

                    sql.executeUpdate(queryStock);
                }
                queryCustomerCreditHistoryOne = "INSERT INTO `customer_credit_history` ("
                        + "`customer_id`, `dr_amount`, `cr_amount`, `transaction_date`, "
                        + "`payment_methods_id`, `cheque_number`, `bank_name`, "
                        + "`branch_name`) VALUES ("
                        + vectorProductSales.get(0).getCustomer().getcustomerId() + ", 0, "
                        + product.getGrandTotal() + ", '"
                        + vectorProductSales.get(0).getData() + "', '"
                        + product.getPaymentMethod().getId() + "', '"
                        + product.getPaymentMethod().getChequeNumber() + "', '"
                        + product.getPaymentMethod().getBankName() + "', '"
                        + product.getPaymentMethod().getBranchName() + "');";

                sql.executeUpdate(queryCustomerCreditHistoryOne);

                queryCustomerCreditHistoryTwo = "INSERT INTO `customer_credit_history` ("
                        + "`customer_id`, `dr_amount`, `cr_amount`, `transaction_date`, "
                        + "`payment_methods_id`, `cheque_number`, `bank_name`, "
                        + "`branch_name`) VALUES ("
                        + vectorProductSales.get(0).getCustomer().getcustomerId() + ", "
                        + product.getReceivedAmount() + ", 0, '"
                        + vectorProductSales.get(0).getData() + "', '"
                        + product.getPaymentMethod().getId() + "', '"
                        + product.getPaymentMethod().getChequeNumber() + "', '"
                        + product.getPaymentMethod().getBankName() + "', '"
                        + product.getPaymentMethod().getBranchName() + "');";

                rowsAffectedTwo = sql.executeUpdate(queryCustomerCreditHistoryTwo);
                sql.commit();
                if (rowsAffectedOne > 0 && rowsAffectedTwo > 0) {
                    rowsAffected = 1;
                }
            } else {
                JOptionPane.showMessageDialog(null, "Some error occur in data insertion");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return rowsAffected;
    }

    public int enterCustomerPayment(CustomerPayment customerPayment) {
        String query = "INSERT INTO `customer_credit_history` ("
                + "`customer_id`, `dr_amount`, `cr_amount`, `transaction_date`, "
                + "`payment_methods_id`, `cheque_number`, `bank_name`, "
                + "`branch_name`) VALUES ("
                + customerPayment.getCustomer().getcustomerId() + ", "
                + customerPayment.getCustomerAmountPay() + ", 0, '"
                + customerPayment.getDate() + "', '"
                + customerPayment.getPaymentMethod().getType() + "', '"
                + customerPayment.getPaymentMethod().getChequeNumber() + "', '"
                + customerPayment.getPaymentMethod().getBankName() + "', '"
                + customerPayment.getPaymentMethod().getBranchName() + "');";

        int rowsAffected = 0;
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);

        try {
            rowsAffected = sql.executeUpdate(query);
            sql.commit();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return rowsAffected;
    }

    public Vector<CustomerPayment> getVectorCustomerPaymentHistory(Customer customer) {
        Vector<CustomerPayment> vectorCustomerPaymentHistory = new Vector<>();
        String query = "SELECT DATE_FORMAT(`transaction_date`, '%d %b %y') AS `transaction_date`, `dr_amount`, `payment_methods_id`, "
                    + "`cheque_number`, `bank_name`, `branch_name` \n "
                    + "FROM `customer_credit_history` \n "
                    + "WHERE `dr_amount` > 0 AND `customer_id` = "
                    + customer.getcustomerId() + ";";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        CustomerPayment customerPayment;
        try {
            resultSet = sql.executeQuery(query);
            int serialNumber = 0;
            while (resultSet.next()) {
                customerPayment = new CustomerPayment();
                serialNumber = serialNumber + 1;
                customerPayment.setSerialNumber(serialNumber);
                customerPayment.setDate(resultSet.getString("transaction_date"));
                customerPayment.setCustomerAmountPay(resultSet.getInt("dr_amount"));
                PaymentMethod paymentMethod = new PaymentMethod(resultSet.getString("payment_methods_id"), resultSet.getString("payment_methods_id"));
                paymentMethod.setChequeNumber(resultSet.getString("cheque_number"));
                paymentMethod.setBankName(resultSet.getString("bank_name"));
                paymentMethod.setBranchName(resultSet.getString("branch_name"));
                customerPayment.setPaymentMethod(paymentMethod);
                customerPayment.setCustomer(customer);
                vectorCustomerPaymentHistory.add(customerPayment);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return vectorCustomerPaymentHistory;
    }
}
