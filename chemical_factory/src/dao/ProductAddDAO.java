package dao;

import entity.Product;
import entity.ProductColor;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import javax.swing.JOptionPane;
import util.SQLQueryUtil;

/**
 *
 * @author rajaa
 */
public class ProductAddDAO {

    public int registerProduct(Product product) {
        String query = "INSERT INTO `products` (`product_name`) VALUES ('"
                + product.getProductName() + "');";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        int rowsAffected = 0;
        try {
            rowsAffected = sql.executeUpdate(query);
            sql.commit();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }

        return rowsAffected;
    }

    public int updateProduct(Product product) {
        String query = "UPDATE `products` SET `product_name` = '"
                + product.getProductName() + "' \n WHERE `id` = "
                + product.getProductId() + ";";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        int rowsAffected = 0;
        try {
            rowsAffected = sql.executeUpdate(query);
            sql.commit();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }

        return rowsAffected;
    }

    public int addProductDetail(Product product) {
        String query = "INSERT INTO `product_details` (`products_id`, `product_size`, "
                + "`product_color`, `price_per_unit`) VALUES ( "
                + product.getProductId() + ", '"
                + product.getSize() + "', '"
                + product.getColor() + "', "
                + product.getPricePerKg() + ");";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        int rowsAffected = 0;
        try {
            rowsAffected = sql.executeUpdate(query);
            sql.commit();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }

        return rowsAffected;
    }

    public int updateProductDetail(Product product) {
        String query = "UPDATE `product_details` SET `product_size` = '"
                + product.getSize() + "', `product_color` = '"
                + product.getColor() + "', `price_per_unit` = "
                + product.getPricePerKg() + " WHERE `id` = "
                + product.getProductDetailId() + ";";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        int rowsAffected = 0;
        try {
            rowsAffected = sql.executeUpdate(query);
            sql.commit();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }

        return rowsAffected;

    }

    public Vector<String> getProductSizes(Product product) {

        Vector<String> vectorProductSize = new Vector<>();

        String query = "SELECT DISTINCT(`product_size`) AS `product_size` FROM `product_details` WHERE `products_id` ="
                + product.getProductId() + ";";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        try {
            resultSet = sql.executeQuery(query);
            while (resultSet.next()) {
                vectorProductSize.add(resultSet.getString("product_size"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }

        return vectorProductSize;
    }

    public Vector<ProductColor> getProductColors(Product product) {

        Vector<ProductColor> vectorProductColors = new Vector<>();

        String query = "SELECT `id`, `product_color` FROM `product_details` WHERE \n"
                + " `products_id` = " + product.getProductId() + " AND `product_size` = '"
                + product.getSize() + "';";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        ProductColor productColor;
        try {
            resultSet = sql.executeQuery(query);
            while (resultSet.next()) {
                productColor = new ProductColor(resultSet.getInt("id"), resultSet.getString("product_color"));

                vectorProductColors.add(productColor);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return vectorProductColors;
    }

    public int registerProductCostDetail(Product product) {
        String queryProductCost = "INSERT INTO `product_cost` (`product_details_id`, `bill`, "
                + "`sacks`, `color`, `chemical`, `label`, `mechanic`, `total_expense`,"
                + " `cost_year_month`, `insertion_date_time`) VALUES ("
                + product.getProductColor().getProductDetailId() + ", "
                + product.getBill() + ", "
                + product.getSacks() + ", "
                + product.getColors() + ", "
                + product.getChemical() + ", "
                + product.getLabel() + ", "
                + product.getMachanic() + ", "
                + product.getTotalExpense() + ", "
                + product.getCostYearMonth() + " , `getCurrentDateTime`());";
        
        String queryProductProduced = "";
        String queryStock = "";
        String queryStockCheck = "";
        String queryMax = "SELECT MAX(`id`) AS `max_id` FROM `product_cost`;";
        
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        int maxId = 0;
        int rowsAffected = 0;
        int count = 0;
        try {
            rowsAffected = sql.executeUpdate(queryProductCost);
            if (rowsAffected > 0) {
                resultSet = sql.executeQuery(queryMax);
                resultSet.next();
                maxId = resultSet.getInt("max_id");
                queryProductProduced = "INSERT INTO `products_produced` (`product_cost_id`, "
                        + "`insertion_date_time`, `quantity_produced`, `cost_price`) VALUES("
                        + maxId + ", `getCurrentDateTime`(), "
                        + product.getProducedProduct() + ", "
                        + product.getPricePerKg() + ")";
                sql.executeUpdate(queryProductProduced);
                queryStockCheck = "SELECT COUNT(*) AS `count` FROM `stock` "
                        + "WHERE `product_details_id` = "
                        + product.getProductColor().getProductDetailId() + ";";
                resultSet = sql.executeQuery(queryStockCheck);
                resultSet.next();
                count = resultSet.getInt("count");
                if(count > 0) {
                    queryStock = "UPDATE `stock` SET `quantity` = `quantity` + "+ product.getProducedProduct() +" " 
                            + "WHERE `product_details_id` = "
                            + product.getProductColor().getProductDetailId() + ";";
                } else {
                    queryStock = "INSERT INTO `stock`(`product_details_id`, `Quantity`) "
                            + "VALUES ("
                            + product.getProductColor().getProductDetailId() + ", "
                            + product.getProducedProduct() + ");";
                }
                rowsAffected = sql.executeUpdate(queryStock);
                sql.commit();
                
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return rowsAffected;

    }
    
    public int getProductQuantity(Product product) {
        String queryProductDetail = "SELECT `id` FROM `product_details` "
                + "WHERE `products_id` = "
                + product.getProductId() + " AND `product_size` = '"
                + product.getSize() + "' AND `product_color` = '"
                + product.getColor() + "';";
        String queryProductQuantity = "";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        int availableQuantity = 0;
        int productDetailId = 0;
        try {
            resultSet = sql.executeQuery(queryProductDetail);
            resultSet.next();
            productDetailId = resultSet.getInt("id");
            queryProductQuantity = "SELECT IFNULL ( SUM(`quantity`), 0) AS `quantity` \n" +
                               "FROM `stock` WHERE `product_details_id` = "
                                + productDetailId + ";";
            resultSet = sql.executeQuery(queryProductQuantity);
            resultSet.next();
            availableQuantity = resultSet.getInt("quantity");
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return availableQuantity;
    }

}
