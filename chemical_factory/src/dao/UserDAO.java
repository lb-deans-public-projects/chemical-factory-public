/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.User;
import java.sql.ResultSet;
import java.sql.SQLException;
import util.SQLQueryUtil;

/**
 *
 * @author Babu Khan
 */
public class UserDAO {

    public int registerUser(User user) {
        int rowsAffected = 0;
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        String query = "";

        try {
            query = "INSERT INTO `users`(`display_name`, `user_name`, `password`)\n"
                    + " VALUES('" + user.getDisplayName() + "','" + user.getUserName() + "',"
                    + "'" + user.getPassword() + "');";
            System.out.println(query);
            rowsAffected = sql.executeUpdate(query);
            sql.commit();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return rowsAffected;
    }

    public int updateUser(User user) {
        String queryOne = "UPDATE `users` SET `display_name`= '"
                + user.getDisplayName() + "', `user_name`= '"
                + user.getUserName() + "' WHERE `id` = "
                + user.getUserId() + ";";
        String password = user.getPassword();
        String queryTwo = "UPDATE `users` SET `display_name`= '"
                + user.getDisplayName() + "', `user_name`= '"
                + user.getUserName() + "', `password`= '"
                + password + "' \n WHERE `id` = "
                + user.getUserId() + ";";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        int rowsAffected = 0;

        try {
            if (password.equals("")) {
                rowsAffected = sql.executeUpdate(queryOne);
                sql.commit();
            } else {
                rowsAffected = sql.executeUpdate(queryTwo);
                sql.commit();
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return rowsAffected;
    }

    public User tryLogin(String userName, String password) {
        User user = null;

        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);

        String query = "SELECT COUNT(*) AS `count`, `display_name`, `id`, `user_name` FROM `users`\n"
                + "WHERE `user_name`='" + userName + "' AND `password`='" + password + "';";

        ResultSet resultSet;
        try {
            resultSet = sql.executeQuery(query);
            resultSet.next();
            
            int count = resultSet.getInt("count");
            
            if(count == 1) {
                user = new User();
                user.setUserId(resultSet.getInt("id"));
                user.setDisplayName(resultSet.getString("display_name"));
                user.setUserName(resultSet.getString("user_name"));
            }
        } catch(SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        
        return user;

    }
}
