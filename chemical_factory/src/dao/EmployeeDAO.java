/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.Employee;
import entity.EmployeeSalary;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import util.SQLQueryUtil;

/**
 *
 * @author Irfan Ullah
 */
public class EmployeeDAO {

    public int registerEmployee(Employee employee) {
        int rowAffectedOne = 0;
        int rowsAffectedTwo = 0;
        SQLQueryUtil sql = new SQLQueryUtil();
        String queryOne;
        String queryTwo;
        sql.connect(false);
        try {
            queryOne = "INSERT INTO `employees`(`employee_name`, `address`, `date_of_birth`, `phone_number`, `cnic_passport_number`, `employee_number`) \n "
                    + "VALUES ('"
                    + employee.getEmployeeName() + "', '"
                    + employee.getAddress() + "', '"
                    + employee.getDateOfBirth() + "', '"
                    + employee.getPhoneNumber() + "', '"
                    + employee.getCnicPassport() + "', "
                    + employee.getEmployeeNumber() + ");";
            rowAffectedOne = sql.executeUpdate(queryOne);

            if (rowAffectedOne > 0) {
                String queryMax = "SELECT MAX(`id`) AS `id` FROM `employees`;";

                ResultSet resultSet = sql.executeQuery(queryMax);
                resultSet.next();
                int maxId = resultSet.getInt("id");
                System.out.println(queryMax);

                queryTwo = "INSERT INTO `employee_salary_definition`(`employees_id`, `basic_pay`, `insertion_date_time`) "
                        + "VALUES (" + maxId + ","
                        + employee.getBasicPay() + ",`getCurrentDateTime`());";
                System.out.println(queryTwo);
                rowsAffectedTwo = sql.executeUpdate(queryTwo);
                if (rowsAffectedTwo > 0) {
                    sql.commit();

                } else {
                    JOptionPane.showMessageDialog(null, "Error in salary insertion");
                }

            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return rowsAffectedTwo;
    }

    public int updateEmployee(Employee employee) {
        String queryOne = "UPDATE `employees` SET `employee_name`= '"
                + employee.getEmployeeName() + "', `address`= '"
                + employee.getAddress() + "', `date_of_birth`= '"
                + employee.getDateOfBirth() + "', `phone_number`= '"
                + employee.getPhoneNumber() + "', `cnic_passport_number`= '"
                + employee.getCnicPassport() + "', `employee_number`= '"
                + employee.getEmployeeNumber() + "' WHERE `id` = "
                + employee.getEmployeeId() + ";";
        String queryTwo = "UPDATE `employee_salary_definition` SET `basic_pay`= "
                + employee.getBasicPay() + " WHERE `employees_id`= "
                + employee.getEmployeeId() + ";";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        int rowsAffectedOne = 0;
        int rowsAffectedtwo = 0;

        try {
            rowsAffectedOne = sql.executeUpdate(queryOne);
            if (rowsAffectedOne > 0) {
                rowsAffectedtwo = sql.executeUpdate(queryTwo);
                sql.commit();
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return rowsAffectedtwo;
    }

    public int employeeAdvance(EmployeeSalary employeeSalary) {
        String query = "INSERT INTO `employee_advance` (`employees_id`, `amount`, `date_of_advance`, `transaction_year_month`) \n "
                + "VALUES ("
                + employeeSalary.getEmployee().getEmployeeId() + ","
                + employeeSalary.getEmployeeAdvanceAmount() + ",'"
                + employeeSalary.getSelectDate() + "',"
                + employeeSalary.getYearMonth() + ");";

        System.out.println("query advance: " + query);
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        int rowsAffected = 0;

        try {
            rowsAffected = sql.executeUpdate(query);
            sql.commit();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return rowsAffected;
    }

    public int employeeAbsentee(EmployeeSalary employeeSalary) {
        int rowsAffected = 0;
        String query = "INSERT INTO `employee_absentee` (`employees_id`, `number_of_days`, `insertion_date_time`, `absentee_year_month`)\n "
                + "VALUES ("
                + employeeSalary.getEmployee().getEmployeeId() + ","
                + employeeSalary.getNumberOfDay() + ","
                + "`getCurrentDateTime`(),"
                + employeeSalary.getYearMonth() + ")";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);

        try {
            rowsAffected = sql.executeUpdate(query);
            sql.commit();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return rowsAffected;
    }

    public int employeeOvertime(EmployeeSalary employeeSalary) {
        String query = "INSERT INTO `employee_overtime` (`employees_id`, `number_of_hours`, `insertion_date_time`, `year_month`)\n"
                + "VALUES ("
                + employeeSalary.getEmployee().getEmployeeId() + ", "
                + employeeSalary.getNumberOfHours() + ", `getCurrentDateTime`(), "
                + employeeSalary.getYearMonth() + ");";

        System.out.println("insertion query overtime: " + query);
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        int rowsAffected = 0;

        try {
            rowsAffected = sql.executeUpdate(query);
            sql.commit();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return rowsAffected;
    }

    public EmployeeSalary getEmployeeSalaryDetails(int employeeId, String selectedDate) {
        EmployeeSalary employeeSalary = new EmployeeSalary();

        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);

        String yearMonth = "";
        String yearMonthArray[] = selectedDate.split("-");
        yearMonth = yearMonthArray[0] + yearMonthArray[1];

        String query = "SELECT IFNULL(SUM(`amount`), 0) AS `total` FROM `employee_advance` WHERE\n"
                + "`employees_id`=" + employeeId + " AND `transaction_year_month`=" + yearMonth + "\n"
                + "UNION ALL\n"
                + "SELECT IFNULL(SUM(`number_of_days`), 0) AS `total` FROM `employee_absentee`\n"
                + "WHERE `employees_id`=" + employeeId + " AND `absentee_year_month`=" + yearMonth + "\n"
                + "UNION ALL\n"
                + "SELECT IFNULL(SUM(`number_of_hours`), 0) AS `total` FROM `employee_overtime`\n"
                + "WHERE `employees_id`=" + employeeId + " AND `year_month`=" + yearMonth + ";";
        ResultSet resultSet;
        System.out.println(query);
        try {
            resultSet = sql.executeQuery(query);
            System.out.println("query executed");
            resultSet.next();
            employeeSalary.setEmployeeAdvanceAmount(resultSet.getInt("total"));
            
            resultSet.next();
            employeeSalary.setNumberOfDay(resultSet.getInt("total"));
            
            resultSet.next();
            employeeSalary.setNumberOfHours(resultSet.getInt("total"));
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }

        return employeeSalary;

    }
    
    public int registerEmployeeSalaryHistory(EmployeeSalary employeeSalary) {
        String query = "INSERT INTO `employee_salary_history` (`employees_id`, `total_absentees`, "
                + "`over_time_hours`, `basic_pay`, `total_absentee_amount`, "
                + "`total_over_time_amount`, `current_month_advance`, `net_salary`, "
                + "`salary_year_month`, `insertion_date_time`, `over_time_rate`) "
                + "VALUES ("
                + employeeSalary.getEmployee().getEmployeeId() + ", "
                + employeeSalary.getNumberOfDay() + ", "
                + employeeSalary.getNumberOfHours() + ", "
                + employeeSalary.getEmployee().getBasicPay() + ", "
                + employeeSalary.getTotalAbsenteeAmount() + ", "
                + employeeSalary.getTotalOvertimeAmount() + ", "
                + employeeSalary.getEmployeeAdvanceAmount() + ", "
                + employeeSalary.getNetSalary() + ", "
                + employeeSalary.getYearMonth() + ", `getCurrentDateTime`(), "
                + employeeSalary.getOverTimeRate() + ");";
        
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        int rowsAffected = 0;
        
        try {
            rowsAffected = sql.executeUpdate(query);
            sql.commit();
        } catch(SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return rowsAffected;
    }
}
