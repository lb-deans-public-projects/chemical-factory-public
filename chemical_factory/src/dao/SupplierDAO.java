package dao;

import entity.Item;
import entity.Supplier;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import javax.swing.JOptionPane;
import util.SQLQueryUtil;

public class SupplierDAO {

    public SupplierDAO() {
    }

    public int registerSupplier(Supplier supplier) {
        String query = "INSERT INTO `suppliers`(`supplier_name`, `address`, `phone_number`, `cnic`) \n"
                + "VALUES ('"
                + supplier.getSupplierName() + "', '"
                + supplier.getAddress() + "', '"
                + supplier.getPhoneNumber() + "', '"
                + supplier.getCnicPassport() + "');";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        int rowsAffected = 0;
        try {
            rowsAffected = sql.executeUpdate(query);
            sql.commit();
        } catch(SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return rowsAffected;
    }
    
    public int updateSupplier(Supplier supplier) {
        String query = "UPDATE `suppliers` SET `supplier_name`= '"
                + supplier.getSupplierName()+ "', `address`= '"
                + supplier.getAddress()+ "', `phone_number`= '"
                +supplier.getPhoneNumber()+ "', `cnic`= '"
                +supplier.getCnicPassport()+ "' WHERE `id` = "
                +supplier.getSupplierId()+ ";";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        int rowAffected = 0;
        try{
            rowAffected = sql.executeUpdate(query);
            sql.commit();
        }catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return rowAffected;
    }
    
    public int registerPurchasesHistory(Vector<Item> vectorPurchases, Item item) {
        int grandTotal = item.getGrandTotal();
        int paidAmount = item.getPayAmount();
        int balance = grandTotal - paidAmount;
        String purchasesQuery = "INSERT INTO `purchases` (`suppliers_id`, `total_bill`, "
                + "`paid_amount`, `balance`, `payment_methods_id`, `cheque_number`, "
                + "`bank_name`, `branch_name`, `transaction_date`, `insertion_date_time`) "
                + "VALUES ("
                + vectorPurchases.get(0).getSupplier().getSupplierId() + ", "
                + item.getGrandTotal() + ", "
                + item.getPayAmount() + ", "
                + balance + ", '"
                + item.getPaymentMethod().getId() + "', '"
                + item.getPaymentMethod().getChequeNumber() + "', '"
                + item.getPaymentMethod().getBankName() + "', '"
                + item.getPaymentMethod().getBranchName() + "', '"
                + vectorPurchases.get(0).getTransactionDate() + "', `getCurrentDateTime`());";
        String queryMax = "SELECT MAX(`id`) AS `max_id` FROM `purchases`;";
        String purchasesDetailQuery = "";
        String supplierCreditDebitQueryOne = "";
        String supplierCreditDebitQueryTwo = "";
        int maxId = 0;
        int i;
        ResultSet resultSet;
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        int rowsAffected = 0;
        int rowsAffectedOne = 0;
        int rowsAffectedTwo = 0;
        int rowsAffectedThree = 0;
        try {
            System.out.println("1");
            rowsAffectedOne = sql.executeUpdate(purchasesQuery);
            System.out.println("2");
            resultSet = sql.executeQuery(queryMax);
            resultSet.next();
            maxId = resultSet.getInt("max_id");
            for(i = 0; i < vectorPurchases.size(); i++) {
                purchasesDetailQuery = "INSERT INTO `purchases_details` (`purchases_id`, "
                        + "`raw_material_items_id`, `quantity`, `unit_price`) VALUES ("
                        + maxId + ", "
                        + vectorPurchases.get(i).getItemId() + ", "
                        + vectorPurchases.get(i).getQuantity() + ", "
                        + vectorPurchases .get(i).getPricePerKg() + ");";
                System.out.println("3");
                sql.executeUpdate(purchasesDetailQuery);
            }
            supplierCreditDebitQueryOne = "INSERT INTO `supplier_credit_purchase_history` "
                    + "(`suppliers_id`, `dr_amount`, `cr_amount`, `transaction_date`, `payment_methods_id`) "
                    + "VALUES ("
                    + vectorPurchases.get(0).getSupplier().getSupplierId() + ", 0, "
                    + item.getGrandTotal() + ", '"
                    + vectorPurchases.get(0).getTransactionDate() + "', '"
                    + item.getPaymentMethod().getId() + "');";
            
            supplierCreditDebitQueryTwo = "INSERT INTO `supplier_credit_purchase_history` "
                    + "(`suppliers_id`, `dr_amount`, `cr_amount`, `transaction_date`, "
                    + "`payment_methods_id`, `cheque_number`, `bank_name`, `branch_name`) "
                    + "VALUES ("
                    + vectorPurchases.get(0).getSupplier().getSupplierId() + ", "
                    + item.getPayAmount()+ ", 0, '"
                    + vectorPurchases.get(0).getTransactionDate() + "', '"
                    + item.getPaymentMethod().getId() + "', '"
                    + item.getPaymentMethod().getChequeNumber() + "', '"
                    + item.getPaymentMethod().getBankName() + "', '"
                    + item.getPaymentMethod().getBranchName() + "');";
            System.out.println("4");
            rowsAffectedTwo = sql.executeUpdate(supplierCreditDebitQueryOne);
            System.out.println("5");
            rowsAffectedThree = sql.executeUpdate(supplierCreditDebitQueryTwo);
            sql.commit();
            if(rowsAffectedOne > 0 && rowsAffectedTwo > 0 && rowsAffectedThree > 0 ) {
                rowsAffected = 1;
            } else {
                JOptionPane.showMessageDialog(null, "Error in data insertion");
            }
            
        } catch(SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return rowsAffected;
    }
    
    public int getSupplierRemainingBalance(Supplier supplier) {
        String query = "SELECT IFNULL(SUM(`cr_amount` - `dr_amount`), 0) AS `total_balance`\n" +
                        "FROM `supplier_credit_purchase_history` \n" +
                         "WHERE `suppliers_id` = "
                        + supplier.getSupplierId() + ";";
        System.out.println(query);
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        int totalBalance = 0;
        ResultSet resultSet;
        try {
            resultSet = sql.executeQuery(query);
            resultSet.next();
            totalBalance = resultSet.getInt("total_balance");
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return totalBalance;
    }
    
    public int addSupplierDebit(Item item) {
        String query = "INSERT INTO `supplier_credit_purchase_history` "
                    + "(`suppliers_id`, `dr_amount`, `cr_amount`, `transaction_date`, "
                    + "`payment_methods_id`, `cheque_number`, `bank_name`, `branch_name`) "
                    + "VALUES ("
                    + item.getSupplier().getSupplierId() + ", "
                    + item.getPayAmount()+ ", 0, '"
                    + item.getTransactionDate() + "', '"
                    + item.getPaymentMethod().getId() + "', '"
                    + item.getPaymentMethod().getChequeNumber() + "', '"
                    + item.getPaymentMethod().getBankName() + "', '"
                    + item.getPaymentMethod().getBranchName() + "');";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        int rowsAffected = 0;
        try {
            rowsAffected = sql.executeUpdate(query);
            sql.commit();
        } catch(SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return rowsAffected;
    }
    
}
