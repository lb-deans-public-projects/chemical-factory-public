/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.Item;
import entity.Supplier;
import java.sql.SQLException;
import util.SQLQueryUtil;

/**
 *
 * @author Haroon Afridi
 */
public class ItemDAO {

    public int registerItem (Item item) {

        String query = "INSERT INTO `raw_material_items` (`item_name`, `suppliers_id`) VALUES ('"
                + item.getItemName() + "'," + item.getSupplier().getSupplierId() + ");";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        int rowsAffected = 0;
        try {
            rowsAffected = sql.executeUpdate(query);
            sql.commit();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }

        return rowsAffected;
    }
    
    public int updateItem(Item item) {
       String query = "UPDATE `raw_material_items` SET `item_name` = '"
               + item.getItemName() + "' WHERE `id` = "
               + item.getItemId() + ";";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        int rowsAffected = 0;
        try {
            rowsAffected = sql.executeUpdate(query);
            sql.commit();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }

        return rowsAffected;
    }

}
