/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author Net Valley
 */
public class CustomerPaymentHistory {
    private Customer customer;
    private int drAmount;
    private int crAmount;
    private String transactionDate;
    private String insertionDateTime;

    public CustomerPaymentHistory() {
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public int getDrAmount() {
        return drAmount;
    }

    public void setDrAmount(int drAmount) {
        this.drAmount = drAmount;
    }

    public int getCrAmount() {
        return crAmount;
    }

    public void setCrAmount(int crAmount) {
        this.crAmount = crAmount;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getInsertionDateTime() {
        return insertionDateTime;
    }

    public void setInsertionDateTime(String insertionDateTime) {
        this.insertionDateTime = insertionDateTime;
    }
    
    
}
