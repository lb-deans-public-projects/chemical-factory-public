/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author coder
 */
public class Customer {
  CustomerPayment customerPayment ;
  
    private int customerId;
    private String customerName;
    private String address;
    private String phoneNumber;
    private String cnic;

    public String getCnic() {
        return cnic;
    }

    public void setCnic(String cnic) {
        this.cnic = cnic;
    }
    private int sNo;

    public int getsNo() {
        return sNo;
    }

    public void setsNo(int sNo) {
        this.sNo = sNo;
    }
    

    public String getName() {
        return customerName;
    }

    public void setName(String customerName) {
        this.customerName = customerName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getcustomerId() {
        return customerId;
    }

    public void setcustomerId(int Id) {
        this.customerId = Id;
    }

    @Override
    public String toString() {
        return customerName;
    }

    
}
