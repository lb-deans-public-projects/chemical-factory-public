/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

public class CustomerPayment {

    private Customer customer;
    private String Date;
    private int customerBalance;
    private int customerAmountPay;
    private String  insertionDateTime;

    private PaymentMethod paymentMethod;
    
    private int serialNumber;

    public int getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(int serialNumber) {
        this.serialNumber = serialNumber;
    }

    
    public String getInsertionDateTime() {
        return insertionDateTime;
    }

    public void setInsertionDateTime(String insertionDateTime) {
        this.insertionDateTime = insertionDateTime;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
    
    
    public CustomerPayment() {
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public int getCustomerBalance() {
        return customerBalance;
    }

    public void setCustomerBalance(int customerBalance) {
        this.customerBalance = customerBalance;
    }

    public int getCustomerAmountPay() {
        return customerAmountPay;
    }

    public void setCustomerAmountPay(int customerAmountPay) {
        this.customerAmountPay = customerAmountPay;
    }

}
