/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author Net Valley
 */
public class Product {
    //declare variables of product details

    private int productId;
    private String productName;
    private String color;
    private String size;
    private int pricePerKg;
    
    private ProductColor productColor;
    private Customer customer;

    
    //declare variables of product cost form      
    private int bill;
    private int sacks;
    private int machanic;
    private int label;
    private int recycle;
    private int chemical;
    private int totalExpense;
    private int colors;
    private String data;
    private int productDetailId;
    private String costYearMonth;
    private int productCostId;

    
    
     private int quantity;
    private int totalAmount;
    
    //variable declear for Producedproduced
    private int producedProduct;
    private int productProducedId;

    private int serialNumber;
    
    //variable declear for Product Sale
    private int grandTotal;
    private int receivedAmount;
    private PaymentMethod paymentMethod;

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
    
    public int getReceivedAmount() {
        return receivedAmount;
    }

    public void setReceivedAmount(int receivedAmount) {
        this.receivedAmount = receivedAmount;
    }
    
    public int getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(int grandTotal) {
        this.grandTotal = grandTotal;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
    
    public int getProductCostId() {
        return productCostId;
    }

    public void setProductCostId(int productCostId) {
        this.productCostId = productCostId;
    }
    
    

    public int getProductProducedId() {
        return productProducedId;
    }

    public void setProductProducedId(int productProducedId) {
        this.productProducedId = productProducedId;
    }

    
    public String getCostYearMonth() {
        return costYearMonth;
    }

    public void setCostYearMonth(String costYearMonth) {
        this.costYearMonth = costYearMonth;
    }

    
    public ProductColor getProductColor() {
        return productColor;
    }

    public void setProductColor(ProductColor productColor) {
        this.productColor = productColor;
    }

    
    public int getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(int serialNumber) {
        this.serialNumber = serialNumber;
    }

    
    public int getProducedProduct() {
        return producedProduct;
    }

    public void setProducedProduct(int producedProduct) {
        this.producedProduct = producedProduct;
    }
    
    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
    

    public int getColors() {
        return colors;
    }

    public void setColors(int colors) {
        this.colors = colors;
    }
    
   

    public Product() {
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getPricePerKg() {
        return pricePerKg;
    }

    public void setPricePerKg(int pricePerKg) {
        this.pricePerKg = pricePerKg;
    }

    @Override
    public String toString() {
        return productName;
    }

    //getter and setter of product cost form

    public int getBill() {
        return bill;
    }

    public void setBill(int bill) {
        this.bill = bill;
    }

    public int getSacks() {
        return sacks;
    }

    public void setSacks(int sacks) {
        this.sacks = sacks;
    }

    public int getMachanic() {
        return machanic;
    }

    public void setMachanic(int machanic) {
        this.machanic = machanic;
    }

    public int getLabel() {
        return label;
    }

    public void setLabel(int label) {
        this.label = label;
    }

    public int getRecycle() {
        return recycle;
    }

    public void setRecycle(int recycle) {
        this.recycle = recycle;
    }

    public int getChemical() {
        return chemical;
    }

    public void setChemical(int chemical) {
        this.chemical = chemical;
    }

    public int getTotalExpense() {
        return totalExpense;
    }

    public int getProductDetailId() {
        return productDetailId;
    }

    public void setProductDetailId(int productDetailId) {
        this.productDetailId = productDetailId;
    }

    public void setTotalExpense(int totalExpense) {
        this.totalExpense = totalExpense;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    
}
