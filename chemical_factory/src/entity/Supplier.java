/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author coder
 */
public class Supplier {
    private int supplierId;
    private String supplierName;
    private String address;
    private String phoneNumber;
    private String cnicPassport;

    
    public Supplier() { //constructor of SupplierRegistration
     
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(int supplierId) {
        this.supplierId = supplierId;
    }

    public String getCnicPassport() {
        return cnicPassport;
    }

    public void setCnicPassport(String cnicPassport) {
        this.cnicPassport = cnicPassport;
    }
    
    @Override
    public String toString() {
        return (supplierName);
    }
    
}
