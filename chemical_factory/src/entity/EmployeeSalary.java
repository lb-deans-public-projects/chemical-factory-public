/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author coder
 */
public class EmployeeSalary {

    private Employee employee;

    private int EmployeeAdvanceAmount;
    private String selectDate;
    private String yearMonth;
    
    //declaration variable for employeeAbsentee for the selected month
    private int numberOfDay;
    
//for Employee Overtime form
    private int numberOfHours;

    private int totalOvertimeAmount;
    private int totalAbsenteeAmount;
    private int netSalary;
    private int overTimeRate;

    public int getOverTimeRate() {
        return overTimeRate;
    }

    public void setOverTimeRate(int overTimeRate) {
        this.overTimeRate = overTimeRate;
    }

    public int getTotalOvertimeAmount() {
        return totalOvertimeAmount;
    }

    public void setTotalOvertimeAmount(int totalOvertimeAmount) {
        this.totalOvertimeAmount = totalOvertimeAmount;
    }

    public int getTotalAbsenteeAmount() {
        return totalAbsenteeAmount;
    }

    public void setTotalAbsenteeAmount(int totalAbsenteeAmount) {
        this.totalAbsenteeAmount = totalAbsenteeAmount;
    }

    public int getNetSalary() {
        return netSalary;
    }

    public void setNetSalary(int netSalary) {
        this.netSalary = netSalary;
    }

    public int getNumberOfHours() {
        return numberOfHours;
    }

    public void setNumberOfHours(int numberOfHours) {
        this.numberOfHours = numberOfHours;
    }

    public int getNumberOfDay() {
        return numberOfDay;
    }

    public void setNumberOfDay(int numberOfDay) {
        this.numberOfDay = numberOfDay;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public int getEmployeeAdvanceAmount() {
        return EmployeeAdvanceAmount;
    }

    public void setEmployeeAdvanceAmount(int EmployeeAdvanceAmount) {
        this.EmployeeAdvanceAmount = EmployeeAdvanceAmount;
    }

    public String getSelectDate() {
        return selectDate;
    }

    public void setSelectDate(String selectDate) {
        this.selectDate = selectDate;
    }

    public String getYearMonth() {
        return yearMonth;
    }

    public void setYearMonth(String yearMonth) {
        this.yearMonth = yearMonth;
    }
    
    

}
