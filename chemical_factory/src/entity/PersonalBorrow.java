/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author Net Valley
 */
public class PersonalBorrow {

    private int borrowId;
    private String borrowName;
    private String phoneNumber;
    
   // private int borrowHistoryId;
    private int debitAmount;
    private int creditAmount;
    private int blance;
    private String selectDate;

    public String getSelectDate() {
        return selectDate;
    }

    public void setSelectDate(String selectDate) {
        this.selectDate = selectDate;
    }

    public int getBorrowId() {
        return borrowId;
    }
     public void setBorrowId(int borrowId) {
        this.borrowId = borrowId;
    }

    public int getBlance() {
        return blance;
    }

    public void setBlance(int blance) {
        this.blance = blance;
    }
    
    public String getBorrowName() {
        return borrowName;
    }

    public void setBorrowName(String borrowName) {
        this.borrowName = borrowName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    // personal borrow history form getter and setter
//    public int getBorrowHistoryId() {
//        return borrowHistoryId;
//    }
//
//    public void setBorrowHistoryId(int borrowHistoryId) {
//        this.borrowHistoryId = borrowHistoryId;
//    }

    public int getDebitAmount() {
        return debitAmount;
    }

    public void setDebitAmount(int debitAmount) {
        this.debitAmount = debitAmount;
    }

    public int getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(int creditAmount) {
        this.creditAmount = creditAmount;
    }

    @Override
    public String toString() {
        return borrowName;
    }
}
