/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author rajaa
 */
public class ProductColor {
    private int productDetailId;
    private String productColor;

    public ProductColor() {
    }

    public ProductColor(int productDetailId, String productColor) {
        this.productDetailId = productDetailId;
        this.productColor = productColor;
    }

    public int getProductDetailId() {
        return productDetailId;
    }

    public void setProductDetailId(int productDetailId) {
        this.productDetailId = productDetailId;
    }

    public String getProductColor() {
        return productColor;
    }

    public void setProductColor(String productColor) {
        this.productColor = productColor;
    }
    
    @Override
    public String toString() {
        return productColor;
    }
}
