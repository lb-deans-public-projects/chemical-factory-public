package services;

import dao.CommonDAO;
import entity.Customer;
import entity.Employee;
import entity.EmployeeSalary;
import entity.Item;
import entity.PaymentMethod;
import entity.Product;
import entity.Supplier;
import entity.User;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import util.SQLQueryUtil;

public class CommonService {
    private CommonDAO commonDAO;

    public CommonService() {
        commonDAO = new CommonDAO();
    }
    
    public Vector<Employee> getVectorEmployee() {
        Vector<Employee> vectorEmplyee = new Vector<>();
        String query = "SELECT `employees`.* , `employee_salary_definition`.* "
                + "FROM `employees`,`employee_salary_definition`\n"
                + "WHERE `employees`.`id` = `employee_salary_definition`.`employees_id` "
                + "ORDER BY `employees`.`id` ASC;";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        Employee employee;
        try {
            resultSet = sql.executeQuery(query);
            sql.commit();
            while (resultSet.next()) {
                int employeeId = resultSet.getInt("id");
                String employeeName = resultSet.getString("employee_name");
                String address = resultSet.getString("address");
                String dateOfBirth = resultSet.getString("date_of_birth");
                String phoneNumber = resultSet.getString("phone_number");
                String cnicPassportNumber = resultSet.getString("cnic_passport_number");
                String employeeNumber = resultSet.getString("employee_number");
                int basicPay = resultSet.getInt("basic_pay");
                employee = new Employee();
                employee.setEmployeeId(employeeId);
                employee.setEmployeeName(employeeName);
                employee.setAddress(address);
                employee.setDateOfBirth(dateOfBirth);
                employee.setPhoneNumber(phoneNumber);
                employee.setCnicPassport(cnicPassportNumber);
                employee.setEmployeeNumber(employeeNumber);
                employee.setBasicPay(basicPay);
                vectorEmplyee.add(employee);

            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return vectorEmplyee;
    }

    public Vector<User> getVectorUsers() {
        Vector<User> vectorUser = new Vector<>();
        String query = "SELECT * FROM `users` ORDER BY `display_name` ASC;";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        User user;
        try {
            resultSet = sql.executeQuery(query);
            sql.commit();
            int serialNumber = 0;
            while (resultSet.next()) {
                int userId = resultSet.getInt("id");
                serialNumber = serialNumber + 1;
                String displayName = resultSet.getString("display_name");
                String userName = resultSet.getString("user_name");
                user = new User();
                user.setUserId(userId);
                user.setSerialNumber(serialNumber);
                user.setUserName(userName);
                user.setDisplayName(displayName);
                vectorUser.add(user);

            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return vectorUser;
    }

    public Vector<Customer> getVectorCustomer() {
        Vector<Customer> vectorCustomer = new Vector<>();
        String query = "SELECT * FROM `customer` ORDER BY `customer_name` ASC;";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        Customer customer;
        try {
            resultSet = sql.executeQuery(query);
            sql.commit();
            while (resultSet.next()) {
                int userId = resultSet.getInt("id");
                String customerName = resultSet.getString("customer_name");
                String customerAddress = resultSet.getString("Address");
                String customerPhoneNumber = resultSet.getString("phone_number");
                String customerCnic = resultSet.getString("cnic");
                customer = new Customer();
                customer.setcustomerId(userId);
                customer.setAddress(customerAddress);
                customer.setPhoneNumber(customerPhoneNumber);
                customer.setCnic(customerCnic);
                customer.setName(customerName);
                vectorCustomer.add(customer);

            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return vectorCustomer;
    }

    public Vector<Supplier> getVectorSupplier() {
        Vector<Supplier> vectorSupplier = new Vector<>();
        String query = "SELECT * FROM `suppliers` ORDER BY `supplier_name` ASC;";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        Supplier supplier;
        try {
            resultSet = sql.executeQuery(query);
            while (resultSet.next()) {
                int supplierId = resultSet.getInt("id");
                String supplierName = resultSet.getString("supplier_name");
                String address = resultSet.getString("address");
                String phoneNumber = resultSet.getString("phone_number");
                String cnicPassport = resultSet.getString("cnic");

                supplier = new Supplier();
                supplier.setSupplierId(supplierId);
                supplier.setSupplierName(supplierName);
                supplier.setAddress(address);
                supplier.setPhoneNumber(phoneNumber);
                supplier.setCnicPassport(cnicPassport);
                vectorSupplier.add(supplier);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return vectorSupplier;
    }

    public Vector<Product> getVectorProduct() {
        Vector<Product> vectorProduct = new Vector<>();
        String query = "SELECT * FROM `products` ORDER BY `product_name` ASC;";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        Product product;
        try {
            resultSet = sql.executeQuery(query);
            sql.commit();
            int serialNumber = 0;
            while (resultSet.next()) {
                int productId = resultSet.getInt("id");
                serialNumber = serialNumber + 1;
                String productName = resultSet.getString("product_name");
                
                product = new Product();
                product.setProductId(productId);
                product.setSerialNumber(serialNumber);
                product.setProductName(productName);
                
                vectorProduct.add(product);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return vectorProduct;
    }

    public Vector<EmployeeSalary> getVectorAllEmployeeAbsentee(Employee employee) {
        Vector<EmployeeSalary> vectorEmployeeAbsentee = new Vector<>();
        System.out.println("2");
        int checkId = employee.getEmployeeId();
        System.out.println("3");
        String QueryOne = "SELECT `em`.`id`,`em`.`employee_name`, `em`.`cnic_passport_number`, "
                + "DATE_FORMAT(`eab`.`insertion_date_time`, '%d %b %y') AS `insertion_date_time`, `eab`.`number_of_days` \n "
                + "FROM `employees` AS `em`, `employee_absentee` AS `eab`\n"
                + "WHERE `em`.`id` = `eab`.`employees_id` AND `eab`.`absentee_year_month` = '"
                + employee.getDate() + "';";
        String QueryTwo = "SELECT `em`.`id`,`em`.`employee_name`, `em`.`cnic_passport_number`, "
                + "DATE_FORMAT(`eab`.`insertion_date_time`, '%d %b %y') AS `insertion_date_time`, `eab`.`number_of_days` \n "
                + "FROM `employees` AS `em`, `employee_absentee` AS `eab`\n"
                + "WHERE `em`.`id` = `eab`.`employees_id` AND `eab`.`absentee_year_month` = '"
                + employee.getDate() + "' AND `em`.`id` = "
                + employee.getEmployeeId() + ";";
        System.out.println("4");
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        EmployeeSalary employeeSalary;
        Employee employeeTwo;
        System.out.println("5");
        try {

            if (checkId == 0) {
                resultSet = sql.executeQuery(QueryOne);
                while (resultSet.next()) {
                    int employeeId = resultSet.getInt("id");
                    String employeeName = resultSet.getString("employee_name");
                    String employeeCnicOrPassport = resultSet.getString("cnic_passport_number");
                    String dateOfAbsentee = resultSet.getString("insertion_date_time");
                    int absentee = resultSet.getInt("number_of_days");
                    employeeSalary = new EmployeeSalary();
                    employeeTwo = new Employee();
                    employeeTwo.setEmployeeId(employeeId);
                    employeeTwo.setEmployeeName(employeeName);
                    employeeTwo.setCnicPassport(employeeCnicOrPassport);
                    employeeSalary.setEmployee(employeeTwo);
                    employeeSalary.setSelectDate(dateOfAbsentee);
                    employeeSalary.setNumberOfDay(absentee);
                    vectorEmployeeAbsentee.add(employeeSalary);
                }
            } else {
                resultSet = sql.executeQuery(QueryTwo);
                while (resultSet.next()) {
                    int employeeId = resultSet.getInt("id");
                    String employeeName = resultSet.getString("employee_name");
                    String employeeCnicOrPassport = resultSet.getString("cnic_passport_number");
                    String dateOfAbsentee = resultSet.getString("insertion_date_time");
                    int absentee = resultSet.getInt("number_of_days");
                    employeeSalary = new EmployeeSalary();
                    employeeTwo = new Employee();
                    employeeTwo.setEmployeeId(employeeId);
                    employeeTwo.setEmployeeName(employeeName);
                    employeeTwo.setCnicPassport(employeeCnicOrPassport);
                    employeeSalary.setEmployee(employeeTwo);
                    employeeSalary.setSelectDate(dateOfAbsentee);
                    employeeSalary.setNumberOfDay(absentee);
                    vectorEmployeeAbsentee.add(employeeSalary);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return vectorEmployeeAbsentee;
    }

    public Vector<EmployeeSalary> getVectorEmployeesAdvance(Employee employee) {
        Vector<EmployeeSalary> vectorEmployeesAdvance = new Vector<>();
        int checkId = employee.getEmployeeId();
        String queryOne = "SELECT `em`.`id`, `em`.`employee_name`, `em`.`cnic_passport_number`, "
                + "DATE_FORMAT(`ead`.`date_of_advance`, '%d %b %Y') AS `date_of_advance`, `ead`.`amount`\n"
                + "FROM `employees` AS `em`, `employee_advance` AS `ead`\n"
                + "WHERE `em`.`id` = `ead`.`employees_id` AND `ead`.`transaction_year_month` = '"
                + employee.getDate() + "';";

        String queryTwo = "SELECT `em`.`id`, `em`.`employee_name`, `em`.`cnic_passport_number`, "
                + "DATE_FORMAT(`ead`.`date_of_advance`, '%d %b %Y') AS `date_of_advance`, `ead`.`amount` \n"
                + "FROM `employees` AS `em`, `employee_advance` AS `ead` \n"
                + "WHERE `em`.`id` = `ead`.`employees_id` AND `ead`.`transaction_year_month` = '"
                + employee.getDate() + "' AND `em`.`id` = "
                + employee.getEmployeeId() + ";";

        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        EmployeeSalary employeeSalary;
        Employee employeeTwo;

        try {

            if (checkId == 0) {
                resultSet = sql.executeQuery(queryOne);
                while (resultSet.next()) {
                    int employeeId = resultSet.getInt("id");
                    String employeeName = resultSet.getString("employee_name");
                    String cnicOrPassport = resultSet.getString("cnic_passport_number");
                    String dateOfAdvance = resultSet.getString("date_of_advance");
                    int advanceAmount = resultSet.getInt("amount");
                    employeeSalary = new EmployeeSalary();
                    employeeTwo = new Employee();
                    employeeTwo.setEmployeeId(employeeId);
                    employeeTwo.setEmployeeName(employeeName);
                    employeeTwo.setCnicPassport(cnicOrPassport);
                    employeeSalary.setEmployee(employeeTwo);
                    employeeSalary.setSelectDate(dateOfAdvance);
                    employeeSalary.setEmployeeAdvanceAmount(advanceAmount);
                    vectorEmployeesAdvance.add(employeeSalary);

                }
            } else {
                resultSet = sql.executeQuery(queryTwo);
                while (resultSet.next()) {
                    int employeeId = resultSet.getInt("id");
                    String employeeName = resultSet.getString("employee_name");
                    String cnicOrPassport = resultSet.getString("cnic_passport_number");
                    String dateOfAdvance = resultSet.getString("date_of_advance");
                    int advanceAmount = resultSet.getInt("amount");
                    employeeSalary = new EmployeeSalary();
                    employee = new Employee();
                    employee.setEmployeeId(employeeId);
                    employee.setEmployeeName(employeeName);
                    employee.setCnicPassport(cnicOrPassport);
                    employeeSalary.setEmployee(employee);
                    employeeSalary.setSelectDate(dateOfAdvance);
                    employeeSalary.setEmployeeAdvanceAmount(advanceAmount);
                    vectorEmployeesAdvance.add(employeeSalary);
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return vectorEmployeesAdvance;
    }

    public Vector<EmployeeSalary> getVectorEmployeesOvertime(EmployeeSalary employeeSalary) {
        System.out.println("3");
        Vector<EmployeeSalary> vectorEmployeeOvertime = new Vector<>();
        System.out.println("4");
        int checkId = employeeSalary.getEmployee().getEmployeeId();
        System.out.println("5");
        String queryOne = "SELECT `em`.`id`, `em`.`employee_name`, `em`.`cnic_passport_number`, "
                + "DATE_FORMAT(`eot`.`insertion_date_time`, '%d %b %Y') AS `insertion_date_time`, `eot`.`number_of_hours`\n"
                + "FROM `employees` AS `em`, `employee_overtime` AS `eot`\n"
                + "WHERE `em`.`id` = `eot`.`employees_id` AND `eot`.`year_month` = '"
                + employeeSalary.getYearMonth() + "';";
        System.out.println("6");
        String queryTwo = "SELECT `em`.`id`, `em`.`employee_name`, `em`.`cnic_passport_number`, "
                + "DATE_FORMAT(`eot`.`insertion_date_time`, '%d %b %Y') AS `insertion_date_time`, `eot`.`number_of_hours`\n"
                + "FROM `employees` AS `em`, `employee_overtime` AS `eot`\n"
                + "WHERE `em`.`id` = `eot`.`employees_id`AND `em`.`id` = "
                + employeeSalary.getEmployee().getEmployeeId() + " AND `eot`.`year_month` = '"
                + employeeSalary.getYearMonth() + "' ORDER BY `em`.`id` ASC;";
        System.out.println("7");
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        Employee employee;

        try {

            if (checkId == 0) {
                resultSet = sql.executeQuery(queryOne);
                while (resultSet.next()) {
                    int employeeId = resultSet.getInt("id");
                    String employeeName = resultSet.getString("employee_name");
                    String cnicOrPassport = resultSet.getString("cnic_passport_number");
                    String dateOfOvertime = resultSet.getString("insertion_date_time");
                    int Overtime = resultSet.getInt("number_of_hours");
                    employeeSalary = new EmployeeSalary();
                    employee = new Employee();
                    employee.setEmployeeId(employeeId);
                    employee.setEmployeeName(employeeName);
                    employee.setCnicPassport(cnicOrPassport);
                    employeeSalary.setEmployee(employee);
                    employeeSalary.setSelectDate(dateOfOvertime);
                    employeeSalary.setNumberOfHours(Overtime);
                    vectorEmployeeOvertime.add(employeeSalary);

                }
            } else {
                resultSet = sql.executeQuery(queryTwo);
                while (resultSet.next()) {
                    int employeeId = resultSet.getInt("id");
                    String employeeName = resultSet.getString("employee_name");
                    String cnicOrPassport = resultSet.getString("cnic_passport_number");
                    String dateOfOvertime = resultSet.getString("insertion_date_time");
                    int Overtime = resultSet.getInt("number_of_hours");
                    employeeSalary = new EmployeeSalary();
                    employee = new Employee();
                    employee.setEmployeeId(employeeId);
                    employee.setEmployeeName(employeeName);
                    employee.setCnicPassport(cnicOrPassport);
                    employeeSalary.setEmployee(employee);
                    employeeSalary.setSelectDate(dateOfOvertime);
                    employeeSalary.setNumberOfHours(Overtime);
                    vectorEmployeeOvertime.add(employeeSalary);

                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return vectorEmployeeOvertime;
    }

    public Vector<Product> getVectorProductDetail() {
        Vector<Product> vectorProductDetail = new Vector<>();
        String query = "SELECT `product_details`.*, `products`.`product_name`\n" +
                "FROM `product_details`, `products` \n" +
                "WHERE `products`.`id` = `product_details`.`products_id` \n" +
                "ORDER BY `products`.`product_name` ASC;";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        Product product;
        
        try {
            resultSet = sql.executeQuery(query);
            int serialNumber = 0;
            while (resultSet.next()) {
                int productDetailId = resultSet.getInt("id");
                serialNumber = serialNumber + 1;
                int productId = resultSet.getInt("products_id");
                String productSize = resultSet.getString("product_size");
                String productColor = resultSet.getString("product_color");
                int pricePerUnit = resultSet.getInt("price_per_unit");
                String productName = resultSet.getString("product_name");
                
                product = new Product();
                product.setProductId(productId);
                product.setProductDetailId(productDetailId);
                product.setSerialNumber(serialNumber);
                product.setSize(productSize);
                product.setColor(productColor);
                product.setPricePerKg(pricePerUnit);
                product.setProductName(productName);
                vectorProductDetail.add(product);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return vectorProductDetail;
    }

    public Vector<Product> getVectorProductCostDetail() {
        return commonDAO.getVectorProductCostDetail();
    }
    
    public Vector<Item> getVectorItems(Supplier supplier) {
        return commonDAO.getVectorItems(supplier);
    }
    
    public Vector<PaymentMethod> getVectorPaymentMethod() {
        return commonDAO.getVectorPaymentMethod();
    }
    
    public int getCustomerRemaingBalance(Customer customer) {
        return commonDAO.getCustomerRemaingBalance(customer);
    }
    
}
