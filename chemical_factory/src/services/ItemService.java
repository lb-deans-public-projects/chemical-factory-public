package services;

import dao.ItemDAO;
import entity.Item;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import util.SQLQueryUtil;

/**
 *
 * @author rajaa
 */
public class ItemService {

    private ItemDAO itemDAO;

    public ItemService() {
        itemDAO = new ItemDAO();
    }
    
    public int registerItem(Item item) {
        String query = "SELECT COUNT(*) AS `count` FROM `raw_material_items` WHERE `item_name` = '"
                + item.getItemName() + "' AND `suppliers_id` = "
                + item.getSupplier().getSupplierId() + ";";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        int count = 0;
        int rowsAffected = 0;
        try {
            resultSet = sql.executeQuery(query);
            resultSet.next();
            count = resultSet.getInt("count");
            if (count > 0) {
                JOptionPane.showMessageDialog(null, "This Item is already registered by this supplier");
            } else {
                rowsAffected = itemDAO.registerItem(item);

            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return rowsAffected;
    }
    
    public int updateItem(Item item) {
        String query = "SELECT COUNT(*) AS `count` FROM `raw_material_items` "
                + "WHERE `item_name` = '"
                + item.getItemName() + "' AND `suppliers_id` = "
                + item.getSupplier().getSupplierId() + " AND `id` != "
                + item.getItemId() + ";";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        int count = 0;
        int rowsAffected = 0;
        try {
            resultSet = sql.executeQuery(query);
            resultSet.next();
            count = resultSet.getInt("count");
            if(count > 0) {
                JOptionPane.showMessageDialog(null, "The provided item name is already registered by this supplier");
            } else {
                rowsAffected = itemDAO.updateItem(item);
            }
            sql.commit();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return rowsAffected;
    }
    
}
