package services;

import dao.ItemDAO;
import dao.SupplierDAO;
import entity.Item;
import entity.Supplier;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import javax.swing.JOptionPane;
import util.SQLQueryUtil;

public class SupplierService {

    private SupplierDAO supplierDao;

    public SupplierService() {
        supplierDao = new SupplierDAO();
    }

    public int registerSupplier(Supplier supplier) {
        String query = "SELECT COUNT(*) AS `count` FROM `suppliers` WHERE `cnic` = '"
                + supplier.getCnicPassport() + "';";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        int rowsAffected = 0;
        int count = 0;
        try {
            resultSet = sql.executeQuery(query);
            sql.commit();
            resultSet.next();
            count = resultSet.getInt("count");
            if (count > 0) {
                JOptionPane.showMessageDialog(null, "Another user is already registered by Cnic OR Passport");
            } else {
                rowsAffected = supplierDao.registerSupplier(supplier);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return rowsAffected;
    }

    public int updateSupplier(Supplier supplier) {
        String query = "SELECT COUNT(*) AS `count` FROM `suppliers` WHERE `cnic` = '"
                + supplier.getCnicPassport() + "' AND `id` != "
                + supplier.getSupplierId() + ";";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        int rowAffected = 0;
        int count = 0;
        try {
            resultSet = sql.executeQuery(query);
            sql.commit();
            resultSet.next();
            count = resultSet.getInt("count");
            if (count > 0) {
                JOptionPane.showMessageDialog(null, "Another user is already registered by this cnic or passport");
            } else {
                rowAffected = supplierDao.updateSupplier(supplier);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return rowAffected;
    }

    public int registerPurchasesHistory(Vector<Item> vectorPurchases, Item item) {
        return supplierDao.registerPurchasesHistory(vectorPurchases, item);
    }
    
    public int getSupplierRemainingBalance(Supplier supplier) {
        return supplierDao.getSupplierRemainingBalance(supplier);
    }
    
    public int addSupplierDebit(Item item) {
        return supplierDao.addSupplierDebit(item);
    }
}
