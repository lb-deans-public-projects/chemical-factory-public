/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import dao.EmployeeDAO;
import entity.Employee;
import entity.EmployeeSalary;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import util.SQLQueryUtil;

/**
 *
 * @author Irfan Ullah
 */
public class EmployeeService {

    private EmployeeDAO employeeDao;

    public EmployeeService() {
        employeeDao = new EmployeeDAO();
    }

    public int registerEmployee(Employee employee) {

        String queryCheck = "SELECT COUNT(*) AS `count` FROM `employees` WHERE `cnic_passport_number` = '"
                + employee.getCnicPassport() + "';";

        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;

        int count = 0;

        int rowsAffected = 0;
        try {
            resultSet = sql.executeQuery(queryCheck);
            resultSet.next();
            count = resultSet.getInt("count");
            if (count > 0) {
                JOptionPane.showMessageDialog(null, "Another user is already register by this cnic or passport");

            } else {
                rowsAffected = employeeDao.registerEmployee(employee);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return rowsAffected;
    }

    public int updateEmployee(Employee employee) {
        String query = "SELECT COUNT(*) AS `count` FROM `employees` \n"
                + "WHERE `cnic_passport_number` = '"
                + employee.getCnicPassport() + "' AND `id` != "
                + employee.getEmployeeId() + "; ";
        System.out.println("count query :" + query);
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        int count = 0;
        int rowsAffected = 0;

        try {
            resultSet = sql.executeQuery(query);
            resultSet.next();
            count = resultSet.getInt("count");
            if (count > 0) {
                JOptionPane.showMessageDialog(null, "Another Employee is already registered by this cnic or passport");
            } else {
                rowsAffected = employeeDao.updateEmployee(employee);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return rowsAffected;
    }

    public int employeeAbsentee(EmployeeSalary employeeSalary) {
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        int count = 0;
        int rowsAffected = 0;
        
        String selectedDate = employeeSalary.getSelectDate();
                
        String yearMonth = employeeSalary.getYearMonth();
        
        String query = "SELECT count(*) as `count` FROM `employee_salary_history` "
                + "WHERE `employees_id`=" + employeeSalary.getEmployee().getEmployeeId()
                + " AND `salary_year_month`=" + yearMonth + ";";
        
        try{
            resultSet =sql.executeQuery(query);
            
            resultSet.next();
            count =resultSet.getInt("count");
            
            if(count > 0) {
                JOptionPane.showMessageDialog(null, "this employee has already taken the salary of the selected month. So absentee in previous history can not be added.");
            } else {
                rowsAffected = employeeDao.employeeAbsentee(employeeSalary);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return rowsAffected;
    
    }

    public int employeeAdvance(EmployeeSalary employeeSalary) {

        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        int count = 0;
        int rowsAffected = 0;

        String selectedDate = employeeSalary.getSelectDate();
        String selectedDateFormatted = selectedDate.replace("-", "");

        int yearMonth = Integer.parseInt(selectedDateFormatted.substring(0, 5));

        String query = "SELECT count(*) as `count` FROM `employee_salary_history` "
                + "WHERE `employees_id`=" + employeeSalary.getEmployee().getEmployeeId()
                + " AND `salary_year_month`=" + yearMonth + ";";

        try {
            resultSet = sql.executeQuery(query);

            resultSet.next();
            count = resultSet.getInt("count");
            if (count > 0) {
                JOptionPane.showMessageDialog(null, "This employee already taken advance of this month.");
            } else {
                rowsAffected = employeeDao.employeeAdvance(employeeSalary);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return rowsAffected;
    }
    
    public int employeeOvertime(EmployeeSalary employeeSalary) {

        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        int count = 0;
        int rowsAffected = 0;

        String query = "SELECT COUNT(*) AS `count` FROM `employee_overtime`\n" +
                "WHERE `employees_id` = "
                + employeeSalary.getEmployee().getEmployeeId()+ " AND `insertion_date_time`= '"
                + employeeSalary.getSelectDate()+ "';";
        System.out.println("count query for overtime"+ query);

        try {
            resultSet = sql.executeQuery(query);

            resultSet.next();
            count = resultSet.getInt("count");
            if (count > 0) {
                JOptionPane.showMessageDialog(null, "Over hour of that employee for this date is already entered");
            } else {
                rowsAffected = employeeDao.employeeOvertime(employeeSalary);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return rowsAffected;
    }

    public EmployeeSalary getEmployeeSalaryDetails(int employeeId, String selectedDate) {
        EmployeeSalary employeeSalary = null;
        
        if(employeeId > 0 && (!(selectedDate.equals("")))) {
            employeeSalary = employeeDao.getEmployeeSalaryDetails(employeeId, selectedDate);
        } else {
            JOptionPane.showMessageDialog(null, "Please select an employee and a date for the month.");
        }
        
        return employeeSalary;
        
    }

    public int registerEmployeeSalaryHistory(EmployeeSalary employeeSalary) {
        String query = "SELECT COUNT(*) AS `count` FROM `employee_salary_history` "
                + "WHERE `employees_id` = "
                + employeeSalary.getEmployee().getEmployeeId() + " AND `salary_year_month` = '"
                + employeeSalary.getYearMonth() + "';";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        int rowsAffected = 0;
        int count = 0;
        
        try {
            resultSet = sql.executeQuery(query);
            resultSet.next();
            count = resultSet.getInt("count");
            if(count > 0) {
                JOptionPane.showMessageDialog(null, "Employee is already taken salary of this month");
            } else {
                rowsAffected = employeeDao.registerEmployeeSalaryHistory(employeeSalary);
            }
        } catch(SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return rowsAffected;
    }
    
    
}
