/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import dao.CustomerDAO;
import entity.Customer;
import entity.CustomerPayment;
import entity.Product;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import javax.swing.JOptionPane;
import util.SQLQueryUtil;

/**
 *
 * @author Haroon Afridi
 */
public class CustomerServices {

    private CustomerDAO customerDAO;
    

    public CustomerServices() {
        customerDAO = new CustomerDAO();
      
    }

    public int customerRegistration(Customer customer) {
         String queryCheck = "SELECT COUNT(*) AS `count` FROM `customer` WHERE `cnic` = '"
                + customer.getCnic()+ "';";

        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        int count = 0;
        int rowsAffected = 0;
        
        try {
            
            resultSet = sql.executeQuery(queryCheck);
            resultSet.next();
            count = resultSet.getInt("count");
            
            if (count > 0) {
                JOptionPane.showMessageDialog(null, "Another customer is already register by this cnic");
                
            } else {
                

                rowsAffected = customerDAO.registerCustomer(customer);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return rowsAffected;

    }

    public int updateCustomer(Customer customer) {

        String queryCheck = "SELECT COUNT(*) AS `count` FROM `customer` WHERE `cnic` = '"
                + customer.getCnic() + "' AND `id` != "
                + customer.getcustomerId() + " ;";
        System.out.println(queryCheck);
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        int rowsAffected = 0;
        int count = 0;

        try {
            resultSet = sql.executeQuery(queryCheck);
            resultSet.next();
            count = resultSet.getInt("count");
            if (count > 0) {
                JOptionPane.showMessageDialog(null, "Another customer is already register by this cnic");

            } else {
                rowsAffected = customerDAO.updateCustomer(customer);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();

        }
        return rowsAffected;

    }
    
    public int enterDailySales(Vector<Product> vectorProductSales, Product product) {
        return customerDAO.enterDailySales(vectorProductSales, product);
    }
    
    public int enterCustomerPayment(CustomerPayment customerPayment) {
        return customerDAO.enterCustomerPayment(customerPayment);
    }
    
    public Vector<CustomerPayment> getVectorCustomerPaymentHistory(Customer customer) {
        return customerDAO.getVectorCustomerPaymentHistory(customer);
    }

}
