package services;

import dao.ProductAddDAO;
import entity.Product;
import entity.ProductColor;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import javax.swing.JOptionPane;
import util.SQLQueryUtil;

/**
 *
 * @author rajaa
 */
public class ProductAddService {

    private ProductAddDAO productAddDAO;

    public ProductAddService() {
        productAddDAO = new ProductAddDAO();
    }

    public int registerProduct(Product product) {
        String query = "SELECT COUNT(*) AS `count` FROM `products` \n"
                + "WHERE `product_name` = '"
                + product.getProductName() + "';";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        int count = 0;
        int rowsAffected = 0;

        try {
            resultSet = sql.executeQuery(query);
            resultSet.next();
            count = resultSet.getInt("count");

            if (count > 0) {
                JOptionPane.showMessageDialog(null, "The provided product name is already exist.");
            } else {
                rowsAffected = productAddDAO.registerProduct(product);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }

        return rowsAffected;
    }

    public int updateProduct(Product product) {
        String query = "SELECT COUNT(*) AS `count` FROM `products` \n"
                + "WHERE `product_name` = '"
                + product.getProductName() + "' AND `id` != "
                + product.getProductId() + ";";
        
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        int count = 0;
        int rowsAffected = 0;

        try {
            resultSet = sql.executeQuery(query);
            resultSet.next();
            count = resultSet.getInt("count");

            if (count > 0) {
                JOptionPane.showMessageDialog(null, "The provided product name is already exist.");
            } else {
                rowsAffected = productAddDAO.updateProduct(product);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }

        return rowsAffected;
    }

    public int addProductDetail(Product product) {
        String query = "SELECT COUNT(*) AS `count` FROM `product_details` \n" +
            "WHERE `product_size` = '"
                + product.getSize() + "' AND `product_color` = '"
                + product.getColor() + "' AND `products_id` = "
                + product.getProductId() + ";";
        
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        int count = 0;
        int rowsAffected = 0;

        try {
            resultSet = sql.executeQuery(query);
            resultSet.next();
            count = resultSet.getInt("count");

            if (count > 0) {
                JOptionPane.showMessageDialog(null, "The provided size and color for this product is already exist.");
            } else {
                rowsAffected = productAddDAO.addProductDetail(product);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }

        
        return rowsAffected;
    }
    
    public int updateProductDetail(Product product) {
        String query = "SELECT COUNT(*) AS `count` FROM `product_details` \n" +
            "WHERE `product_size` = '"
                + product.getSize() + "' AND `product_color` = '"
                + product.getColor() + "' AND `product_id` = "
                + product.getProductId() + " AND `id` != "
                + product.getProductDetailId() + ";";
        
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        int count = 0;
        int rowsAffected = 0;

        try {
            resultSet = sql.executeQuery(query);
            resultSet.next();
            count = resultSet.getInt("count");

            if (count > 0) {
                JOptionPane.showMessageDialog(null, "The provided size and color is already exist.");
            } else {
                rowsAffected = productAddDAO.updateProductDetail(product);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }

        return rowsAffected;
    }
    
    
    public Vector<String> getProductSizes(Product product) {
        return productAddDAO.getProductSizes(product);
    }
    
    public Vector<ProductColor> getProductColors(Product product) {
        return productAddDAO.getProductColors(product);
    }
    
    public int registerProductCostDetail(Product product) {
        return productAddDAO.registerProductCostDetail(product);
    }
    
    public int getProductQuantity(Product product) {
        return productAddDAO.getProductQuantity(product);
    }
    
    
}
