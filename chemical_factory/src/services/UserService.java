/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import dao.UserDAO;
import entity.User;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import util.SQLQueryUtil;

/**
 *
 * @author Babu Khan
 */
public class UserService {

    private UserDAO userDao;

    public UserService() {
        userDao = new UserDAO();
    }
    
    
    public int registerUser(User user) {

        String queryCheck = "SELECT COUNT(*) AS `count` FROM `users`\n WHERE  `user_name` = '"
                + user.getUserName() + "';";

        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;

        int count = 0;

        int rowsAffected = 0;

        try {
            resultSet = sql.executeQuery(queryCheck);
            resultSet.next();
            count = resultSet.getInt("count");

            if (count > 0) {
                JOptionPane.showMessageDialog(null, "The provided user name already exist.");
            } else {
                rowsAffected = userDao.registerUser(user);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }

        return rowsAffected;

    }
    
    public int updateUser(User user) {
        String query = "SELECT COUNT(*) AS `count` FROM `users` \n WHERE `user_name` = '"
                + user.getUserName() + "' AND `id` != "
                + user.getUserId() + ";";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        int count = 0;
        int rowsAffected = 0;
        
        try {
            resultSet = sql.executeQuery(query);
            sql.commit();
            resultSet.next();
            count = resultSet.getInt("count");
            if(count > 0) {
                JOptionPane.showMessageDialog(null, "Another user is already registered by this user name.");
            } else {
                rowsAffected = userDao.updateUser(user);
            }
        } catch(SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return rowsAffected;
    }
    
    public User tryLogin(String userName, String password) {
        User user = null;
        if(userName.equals("") || password.equals("")) {
            JOptionPane.showMessageDialog(null, "username or password can not be empty.");
        } else {
            user = userDao.tryLogin(userName, password);
        }
        return user;
    }
}
